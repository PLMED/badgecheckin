package edu.uky.pml.badge.collector.services;

import edu.uky.pml.badge.collector.models.Swipe;
import edu.uky.pml.badge.collector.utilities.SessionFactoryManager;
import edu.uky.pml.badge.common.SwipeRecord;
import io.cresco.library.plugin.PluginBuilder;
import io.cresco.library.utilities.CLogger;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import java.util.ArrayList;
import java.util.List;

public class SwipeService {
    private static CLogger logger;

    public static void startLogging(PluginBuilder pluginBuilder) {
        logger = pluginBuilder.getLogger(SwipeService.class.getName(), CLogger.Level.Info);
    }

    public static synchronized Swipe create(SwipeRecord swipeRecord) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return null;
        try {
            session.getTransaction().begin();
            Swipe object = new Swipe( swipeRecord );
            session.save( object );
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static List<Swipe> all() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            List<Swipe> list = session.createQuery( "from Swipe", Swipe.class ).list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static Swipe getByID(String id) {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            return null;
        }
        try {
            session.getTransaction().begin();
            Query query = session.createQuery( "from Swipe where id = :id", Swipe.class );
            query.setParameter("id", id);
            Swipe object = (Swipe) query.uniqueResult();
            session.getTransaction().commit();
            return object;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            e.printStackTrace();
            return null;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getCount(String site, String from, String to) {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            return -1L;
        }
        try {
            session.getTransaction().begin();
            String prune = "";
            if (site != null)
                prune = " AND site = :siteName";
            String queryString = "SELECT id FROM swipe WHERE  ts >= :fromTS AND ts <= :toTS" + prune;
            Query query = session.createSQLQuery(queryString);
            query.setParameter("fromTS", Long.valueOf(from));
            query.setParameter("toTS", Long.valueOf(to));
            if (site != null)
                query.setParameter("siteName", site);
            final List list = query.list();
            session.getTransaction().commit();
            return list.size();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            logger.error("getCount() Exception:\n" + ExceptionUtils.getStackTrace(e));
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static long getFilteredCount(String site, String from, String to, String filter) {
        Session session = SessionFactoryManager.getSession();
        if (session == null) {
            return -1L;
        }
        try {
            session.getTransaction().begin();
            String prune = "";
            if (site != null)
                prune = " AND site = :siteName";
            //String queryString = "SELECT s.id as samplesheet_id, s.experiment_name, s.created, u.id as user_id, u.username, GROUP_CONCAT(se.id, ';', se.name SEPARATOR ',') as sequence_list FROM sample_sheet s LEFT JOIN user u on s.owner_id = u.id LEFT JOIN sequence se on se.sample_sheet_id = s.id GROUP BY s.id HAVING (u.username LIKE :filter OR sequence_list LIKE :filter OR s.experiment_name LIKE :filter)" + prune;
            String queryString = "SELECT id FROM swipe WHERE ts >= :fromTS AND ts <= :toTS AND (site LIKE :filter OR ukid LIKE :filter OR LOWER(link_blue) LIKE :filter OR LOWER(full_name) LIKE :filter)" + prune;
            Query query = session.createSQLQuery(queryString);
            query.setParameter("fromTS", Long.valueOf(from));
            query.setParameter("toTS", Long.valueOf(to));
            query.setParameter("filter", "%" + filter.toLowerCase() + "%");
            if (site != null)
                query.setParameter("siteName", site);
            final List list = query.list();
            session.getTransaction().commit();
            return list.size();
        } catch (RuntimeException e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            logger.error("getFilteredCount() Exception:\n" + ExceptionUtils.getStackTrace(e));
            return -1L;
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static List<Object[]> list(String site, String from, String to, String sortBy, String dir, String filter, int start, int length) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            String prune = "";
            if (site != null)
                prune = " AND site = :siteName";
            String queryString = "SELECT id, ts, site, swipe, ukid, link_blue, full_name FROM swipe WHERE ts >= :fromTS AND ts <= :toTS AND (site LIKE :filter OR ukid LIKE :filter OR LOWER(link_blue) LIKE :filter OR LOWER(full_name) LIKE :filter)" + prune + " ORDER BY " + sortBy + " " + dir;
            Query query = session.createSQLQuery(queryString);
            query.setParameter("fromTS", Long.valueOf(from));
            query.setParameter("toTS", Long.valueOf(to));
            query.setParameter("filter", "%" + filter.toLowerCase() + "%");
            if (site != null)
                query.setParameter("siteName", site);
            query.setFirstResult(start);
            query.setMaxResults(length);
            final List list = query.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            if (logger != null)
                logger.error("list({},{},{},{},{},{},{},{})\n" + ExceptionUtils.getStackTrace(e), site, from, to,
                        sortBy, dir, filter, start, length);
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static List<Object[]> download(String site, String from, String to, String filter) {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            String prune = "";
            if (site != null)
                prune = " AND site = :siteName";
            String queryString = "SELECT ts, site, ukid, link_blue, full_name FROM swipe WHERE ts >= :fromTS AND ts <= :toTS AND (site LIKE :filter OR ukid LIKE :filter OR LOWER(link_blue) LIKE :filter OR LOWER(full_name) LIKE :filter)" + prune;
            Query query = session.createSQLQuery(queryString);
            query.setParameter("fromTS", Long.valueOf(from));
            query.setParameter("toTS", Long.valueOf(to));
            query.setParameter("filter", "%" + filter.toLowerCase() + "%");
            if (site != null)
                query.setParameter("siteName", site);
            final List list = query.list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            if (logger != null)
                logger.error("download({},{},{})\n" + ExceptionUtils.getStackTrace(e), site, from, to);
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }

    public static List<Object[]> getSites() {
        Session session = SessionFactoryManager.getSession();
        if (session == null)
            return new ArrayList<>();
        try {
            session.getTransaction().begin();
            String queryString = "SELECT DISTINCT(site) FROM swipe";
            List list = session.createSQLQuery( queryString ).list();
            session.getTransaction().commit();
            return list;
        } catch (Exception e) {
            if (session.getTransaction().getStatus() == TransactionStatus.ACTIVE ||
                    session.getTransaction().getStatus() == TransactionStatus.MARKED_ROLLBACK)
                session.getTransaction().rollback();
            return new ArrayList<>();
        } finally {
            try {
                session.close();
            } catch (HibernateException e) {
                e.printStackTrace();
            }
        }
    }
}
