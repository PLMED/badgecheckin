package edu.uky.pml.badge.collector.models;

import edu.uky.pml.badge.common.SwipeRecord;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table( name = "swipe" )
public class Swipe {
    @Id
    private String id;

    @Column( name = "ts" )
    private Long ts;

    @Column( name = "site" )
    private String site;

    @Column( name = "swipe" )
    private String swipe;

    @Column( name = "ukid" )
    private String ukID;

    @Column( name = "link_blue" )
    private String linkBlue;

    @Column( name = "full_name" )
    private String fullName;

    public Swipe() {
        this.id = java.util.UUID.randomUUID().toString();
    }

    public Swipe(String site, String swipe, String ukID, String ts) {
        this();
        setTs(ts);
        setSite(site);
        setUkID(ukID);
        setSwipe(swipe);
    }

    public Swipe(String site, String swipe, String ukID, String ts, String linkBlue, String fullName) {
        this(site, swipe, ukID, ts);
        setLinkBlue(linkBlue);
        setFullName(fullName);
    }

    public Swipe(String site, String swipe, String ukID, Long ts) {
        this();
        setTs(ts);
        setSite(site);
        setUkID(ukID);
        setSwipe(swipe);
    }

    public Swipe(String site, String swipe, String ukID, Long ts, String linkBlue, String fullName) {
        this(site, swipe, ukID, ts);
        setLinkBlue(linkBlue);
        setFullName(fullName);
    }

    public Swipe(SwipeRecord swipeRecord) {
        this();
        setTs(swipeRecord.getDate());
        setSite(swipeRecord.getSite());
        setSwipe(swipeRecord.getSwipe());
        setUkID(swipeRecord.getId());
        setLinkBlue(swipeRecord.getLinkBlue());
        setFullName(swipeRecord.getFullName());
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public Long getTs() {
        return ts;
    }
    public Date getTsAsDate() {
        return new Date(ts);
    }
    public void setTs(Long ts) {
        this.ts = ts;
    }
    public void setTs(String ts) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ");
        try {
            this.ts = dateFormat.parse(ts).getTime();
        } catch (ParseException e) {
            this.ts = null;
        }
    }

    public String getSite() {
        return site;
    }
    public void setSite(String site) {
        this.site = site;
    }

    public String getSwipe() {
        return swipe;
    }
    public void setSwipe(String swipe) {
        this.swipe = swipe;
    }

    public String getUkID() {
        return ukID;
    }
    public void setUkID(String ukID) {
        this.ukID = ukID;
    }

    public String getLinkBlue() {
        return linkBlue;
    }
    public void setLinkBlue(String linkBlue) {
        this.linkBlue = linkBlue;
    }

    public String getFullName() {
        return fullName;
    }
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
