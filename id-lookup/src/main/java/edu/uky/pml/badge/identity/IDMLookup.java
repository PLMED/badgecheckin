package edu.uky.pml.badge.identity;

import com.google.gson.Gson;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class IDMLookup {
    private String IDM_USER_ID;
    private String IDM_PASSWORD;
    private String IDM_URL;

    private HttpClient httpClient;

    public IDMLookup(String userId, String password, String url) throws IllegalArgumentException {
        if (userId == null)
            throw new IllegalArgumentException("UK identity access username cannot be null");
        this.IDM_USER_ID = userId;
        if (password == null)
            throw new IllegalArgumentException("UK identity access password cannot be null");
        this.IDM_PASSWORD = password;
        if (url == null)
            throw new IllegalArgumentException("UK identity access url cannot be null");
        this.IDM_URL = url;
    }

    public UKIdentity testLookup(String id) throws IDMLookupException {
        HttpResponse httpResponse = getIDMResponseForID(id);
        if (httpResponse.getStatusLine().getStatusCode() == 200) {
            try {
                String json = EntityUtils.toString(httpResponse.getEntity());
                if (json == null)
                    return null;
                Gson gson = new Gson();
                return gson.fromJson(json, UKIdentity.class);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else if (httpResponse.getStatusLine().getStatusCode() == 401) {
            throw new IDMLookupException("Invalid UK identity access credentials provided");
        } else if (httpResponse.getStatusLine().getStatusCode() == 404) {
            throw new IDMLookupException(String.format("User with ID=%s does not exist", id));
        } else {
            throw new IDMLookupException(String.format("Received status code %d response from ID server",
                    httpResponse.getStatusLine().getStatusCode()));
        }
    }

    private HttpResponse getIDMResponseForID(String id) throws IDMLookupException {
        String checkedID = checkID(id);
        HttpGet httpGet = getHttpGet(checkedID);
        if (httpGet == null)
            throw new IDMLookupException("Failed to build authenticated web request");
        HttpClient httpClient = getHttpClient();
        if (httpClient == null)
            throw new IDMLookupException("Failed to build web client");
        try {
            return httpClient.execute(httpGet);
        } catch (ClientProtocolException e) {
            throw new IDMLookupException("Invalid client protocol in UK identity server web request");
        } catch (IOException e) {
            throw new IDMLookupException("Web request encountered a problem or the connection was aborted");
        }
    }

    private String checkID(String id) throws IDMLookupException {
        if (id == null)
            throw new IDMLookupException("UK id cannot be null");
        if (!StringUtils.isNumeric(id))
            throw new IDMLookupException("UK id must contain only numbers");
        if (id.length() < 8)
            throw new IDMLookupException("UK id must be at least 8 numbers long");
        if (id.length() == 9 && !id.startsWith("9"))
            throw new IDMLookupException("9-digit UK ids must start with a 9");
        else if (id.length() == 9)
            return id.substring(1);
        return id;
    }

    private HttpGet getHttpGet(String id) {
        String url = String.format("%s/%s", IDM_URL, id);
        HttpGet httpGet = new HttpGet(url);
        String credentials = IDM_USER_ID + ":" + IDM_PASSWORD;
        byte[] encodedCredentials = Base64.encodeBase64(credentials.getBytes(StandardCharsets.UTF_8));
        String credentialsHeader = "Basic " + new String(encodedCredentials);
        httpGet.setHeader(HttpHeaders.AUTHORIZATION, credentialsHeader);
        return httpGet;
    }

    private HttpClient getHttpClient() {
        if (httpClient == null)
            httpClient = HttpClientBuilder.create().build();
        return httpClient;
    }
}
