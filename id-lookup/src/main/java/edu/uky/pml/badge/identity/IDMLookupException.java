package edu.uky.pml.badge.identity;

public class IDMLookupException extends Exception {
    public IDMLookupException() {
        super();
    }

    public IDMLookupException(String message) {
        super(message);
    }
}
