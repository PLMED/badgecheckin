package edu.uky.pml.badge.display.utilities;

import edu.uky.pml.badge.common.SwipeRecord;

public class DisplayState {
    private static DisplayState instance;
    public static DisplayState getInstance() {
        if (instance == null) {
            instance = new DisplayState();
        }
        return instance;
    }

    private long _version = 0L;
    private boolean _pending = false;
    private boolean _success = false;
    private SwipeRecord _swipe = null;

    public synchronized void update(boolean pending, boolean success, SwipeRecord swipe) {
        _version++;
        _pending = pending;
        _success = success;
        _swipe = swipe;
    }

    public boolean getPending() {
        return _pending;
    }

    public boolean getSuccess() {
        return _success;
    }

    public SwipeRecord getSwipe() {
        return _swipe;
    }

    public long getVersion() {
        return _version;
    }
}
