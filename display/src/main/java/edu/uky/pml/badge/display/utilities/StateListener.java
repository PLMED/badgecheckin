package edu.uky.pml.badge.display.utilities;

import com.google.gson.Gson;
import edu.uky.pml.badge.common.BadgeCheckInStatics;
import edu.uky.pml.badge.common.SwipeRecord;
import io.cresco.library.data.TopicType;
import io.cresco.library.plugin.PluginBuilder;
import io.cresco.library.utilities.CLogger;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class StateListener {
    private PluginBuilder pluginBuilder;
    private CLogger logger;

    private String siteId;
    private String recordListenerId = null;
    private String resultListenerId = null;

    public StateListener(PluginBuilder pluginBuilder) {
        this.pluginBuilder = pluginBuilder;
        this.logger = pluginBuilder.getLogger(StateListener.class.getName(), CLogger.Level.Trace);
        setSiteId(pluginBuilder.getConfig().getStringParam("site_id", pluginBuilder.getAgent()));
    }

    public void start() {
        logger.info("Starting display listeners");
        if (recordListenerId == null) {
            MessageListener ml = (Message msg) -> {
                try {
                    if (msg instanceof TextMessage) {
                        Gson gson = new Gson();
                        TextMessage textMessage = (TextMessage) msg;
                        SwipeRecord swipe = gson.fromJson(textMessage.getText(), SwipeRecord.class);
                        logger.info("Record@Display: {} from {} ({}-{}-{}) at {}", swipe.getId(), swipe.getSite(),
                                swipe.getCrescoRegion(), swipe.getCrescoAgent(), swipe.getCrescoPlugin(),
                                swipe.getDateAsDate());
                        if (swipe.getId() != null) {
                            DisplayState.getInstance().update(true, false, swipe);
                        } else {
                            DisplayState.getInstance().update(false, false, swipe);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            };
            recordListenerId = pluginBuilder.getAgentService().getDataPlaneService().addMessageListener(
                    TopicType.AGENT,
                    ml,
                    String.format(
                            "%s='%s'",
                            BadgeCheckInStatics.SWIPE_RECORD_DATA_PLANE_IDENTIFIER_KEY,
                            BadgeCheckInStatics.getSiteSwipeRecordDataPlaneValue(siteId)
                    )
            );
        }
        if (resultListenerId == null) {
            MessageListener ml = (Message msg) -> {
                try {
                    if (msg instanceof TextMessage) {
                        Gson gson = new Gson();
                        TextMessage textMessage = (TextMessage) msg;
                        SwipeRecord swipe = gson.fromJson(textMessage.getText(), SwipeRecord.class);
                        if (swipe.getId() != null) {
                            logger.info("Result@Display: {} from {} ({}-{}-{}) at {}", swipe.getId(), swipe.getSite(),
                                    swipe.getCrescoRegion(), swipe.getCrescoAgent(), swipe.getCrescoPlugin(),
                                    swipe.getDateAsDate());
                            DisplayState.getInstance().update(false, true, swipe);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            };
            resultListenerId = pluginBuilder.getAgentService().getDataPlaneService().addMessageListener(
                    TopicType.AGENT,
                    ml,
                    String.format(
                            "%s='%s'",
                            BadgeCheckInStatics.SWIPE_RESULT_DATA_PLANE_IDENTIFIER_KEY,
                            BadgeCheckInStatics.getSiteSwipeResultDataPlaneValue(siteId)
                    )
            );
        }
    }

    public void stop() {
        if (recordListenerId != null) {
            pluginBuilder.getAgentService().getDataPlaneService().removeMessageListener(recordListenerId);
        }
        if (resultListenerId != null) {
            pluginBuilder.getAgentService().getDataPlaneService().removeMessageListener(resultListenerId);
        }
    }

    public String getSiteId() {
        return siteId;
    }
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }
}
