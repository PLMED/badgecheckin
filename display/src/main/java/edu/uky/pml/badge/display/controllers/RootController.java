package edu.uky.pml.badge.display.controllers;

import com.google.gson.Gson;
import edu.uky.pml.badge.display.Plugin;
import edu.uky.pml.badge.display.utilities.DisplayState;
import io.cresco.library.utilities.CLogger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("/")
public class RootController {
    private static CLogger logger;
    private static final Gson gson = new Gson();

    public static void initialize() {
        logger = Plugin.pluginBuilder.getLogger(RootController.class.getName(), CLogger.Level.Info);
    }

    public RootController() { }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStatus() {
        try {
            Map<String, Object> ret = new HashMap<>();
            ret.put("pending", DisplayState.getInstance().getPending());
            ret.put("success", DisplayState.getInstance().getSuccess());
            ret.put("swipe", DisplayState.getInstance().getSwipe());
            ret.put("version", DisplayState.getInstance().getVersion());
            return Response.ok(gson.toJson(ret)).build();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getStatus() failure: " + e.getClass() + ":" + e.getMessage());
            return Response.serverError().build();
        }
    }
}
