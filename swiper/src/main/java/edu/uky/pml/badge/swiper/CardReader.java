package edu.uky.pml.badge.swiper;

import com.google.gson.Gson;
import edu.uky.pml.badge.common.BadgeCheckInStatics;
import edu.uky.pml.badge.common.SwipeRecord;
import edu.uky.pml.badge.common.Translator;
import edu.uky.pml.badge.utilities.RollingFileAppender;
import io.cresco.library.data.TopicType;
import io.cresco.library.plugin.PluginBuilder;
import io.cresco.library.utilities.CLogger;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hid4java.*;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class CardReader {
    private PluginBuilder pluginBuilder = null;
    private CLogger logger = null;
    private RollingFileAppender backupFileAppender;

    private String siteId;
    private List<SwipeRecord> records = new ArrayList<>();
    private CardReaderWorker cardReaderWorker = null;
    private Timer heartBeat;

    public CardReader(PluginBuilder pluginBuilder) {
        this.pluginBuilder = pluginBuilder;
        this.logger = pluginBuilder.getLogger(CardReader.class.getName(), CLogger.Level.Trace);
        setSiteId(pluginBuilder.getConfig().getStringParam("site_id", pluginBuilder.getAgent()));
        try {
            backupFileAppender = new RollingFileAppender(Paths.get(pluginBuilder.getConfig().getStringParam("swipe_logs", "swipe_logs")));
        } catch (IllegalArgumentException e) {
            logger.error("Error creating swipe backup log utility: {}", e.getMessage());
        } catch (IOException e) {
            logger.error("Error creating swipe backup log utility: {}", e.getMessage());
            e.printStackTrace();
        }
    }

    public void start() {
        if (cardReaderWorker == null) {
            cardReaderWorker = new CardReaderWorker();
            new Thread(cardReaderWorker).start();
        } else {
            logger.error("Card reader is already active");
        }
        /*if (heartBeat == null) {
            heartBeat = new Timer();
            heartBeat.scheduleAtFixedRate(new CardReaderHeartBeatTask(pluginBuilder), 15000, 5000);
        } else {
            logger.error("Heartbeat is already active");
        }*/
    }

    public void stop() {
        if (cardReaderWorker != null) {
            cardReaderWorker.stop();
            cardReaderWorker = null;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                logger.error("Shutdown sleep interrupted");
            }
        } else {
            logger.error("Card reader is not running");
        }
        /*if (heartBeat != null) {
            heartBeat.cancel();
            heartBeat = null;
        } else {
            logger.error("Heartbeat is not running");
        }*/
    }

    public String getSiteId() {
        return siteId;
    }
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    private void backupSwipe(SwipeRecord swipeRecord) {
        if (backupFileAppender != null && swipeRecord != null)
            try {
                backupFileAppender.append(String.format("%s,%s,%s,%s",
                        swipeRecord.getDateAsDate(),
                        swipeRecord.getSite(),
                        swipeRecord.getSwipe(),
                        swipeRecord.getId()));
            } catch (IOException e) {
                logger.error("Failed to log swipe ({},{},{},{})",
                        swipeRecord.getDateAsDate(),
                        swipeRecord.getSite(),
                        swipeRecord.getSwipe(),
                        swipeRecord.getId());
            }
    }

    private class CardReaderWorker implements Runnable {
        private CLogger logger = null;
        private final Integer VENDOR_ID = 0x0801;
        private final Integer PRODUCT_ID = 0x01;
        private final int PACKET_LENGTH = 8;
        public final String SERIAL_NUMBER = null;
        private boolean running = true;

        public CardReaderWorker() {
            this.logger = pluginBuilder.getLogger(CardReaderWorker.class.getName(), CLogger.Level.Trace);
        }

        public void stop() {
            this.running = false;
        }

        @Override
        public void run() {
            Gson gson = new Gson();
            byte[] piece = new byte[PACKET_LENGTH];
            String data = "";
            try {
                //Thread.sleep(15000);
                HidServicesSpecification hidServicesSpecification = new HidServicesSpecification();
                hidServicesSpecification.setAutoShutdown(true);
                hidServicesSpecification.setScanInterval(500);
                hidServicesSpecification.setPauseInterval(5000);
                hidServicesSpecification.setScanMode(ScanMode.SCAN_AT_FIXED_INTERVAL_WITH_PAUSE_AFTER_WRITE);

                // Get HID services using custom specification
                HidServices hidServices = HidManager.getHidServices(hidServicesSpecification);

                // Start the services
                logger.trace("Starting HID services.");
                //hidServices.start();

                for (HidDevice hidDevice : hidServices.getAttachedHidDevices()) {
                    logger.trace(hidDevice.toString());
                }

                // Open the device device by Vendor ID and Product ID with wildcard serial number
                HidDevice hidDevice = hidServices.getHidDevice(VENDOR_ID, PRODUCT_ID, SERIAL_NUMBER);
                if (hidDevice == null) {
                    logger.error("Magnetic card reader not found, aborting...");
                    return;
                }
                logger.info("Now reading...");
                while (running) {
                    boolean moreData = true;
                    while (moreData) {
                        int val = hidDevice.read(piece, 500);
                        switch (val) {
                            case -1:
                                logger.error(hidDevice.getLastErrorMessage());
                                break;
                            case 0:
                                moreData = false;
                                break;
                            default:
                                String character = Translator.translate(piece);
                                if (character != null) {
                                    if (character.equals("\n")) {
                                        logger.trace("Added swipe: " + data);
                                        /*int idStart = data.indexOf("%") + 1;
                                        int idEnd = data.indexOf("?");
                                        String id = null;
                                        if ((idEnd - idStart) == 9)
                                            id = data.substring(idStart, (idEnd - idStart + 1));*/
                                        int stripeOneStart = data.indexOf("%") + 1;
                                        int stripeOneEnd = data.indexOf("?");
                                        int stripeTwoStart = data.indexOf(";") + 1;
                                        int stripeTwoEnd = data.indexOf("=");
                                        int stripeThreeStart = stripeTwoEnd + 1;
                                        int stripeThreeEnd = data.lastIndexOf("?");
                                        boolean hasStripeOne = stripeOneStart > 0;
                                        int stripeOneLength = stripeOneEnd - stripeOneStart;
                                        boolean hasStripeTwo = (stripeTwoEnd - stripeTwoStart) > 0;
                                        int stripeTwoLength = stripeTwoEnd - stripeTwoStart;
                                        boolean hasStripeThree = stripeThreeStart > 0;
                                        int stripeThreeLength = stripeThreeEnd - stripeThreeStart;
                                        String id = null;
                                        if (hasStripeOne && stripeOneLength == 9)
                                            id = data.substring(stripeOneStart, stripeOneEnd);
                                        else if (hasStripeTwo && stripeTwoLength == 9)
                                            id = data.substring(stripeTwoStart, stripeTwoEnd);
                                        else if (hasStripeThree && stripeThreeLength == 9)
                                            id = data.substring(stripeThreeStart, stripeThreeEnd);
                                        SwipeRecord record = new SwipeRecord(getSiteId(), data, id,
                                                pluginBuilder.getRegion(), pluginBuilder.getAgent(), pluginBuilder.getPluginID());
                                        records.add(record);
                                        backupSwipe(record);
                                        logger.info("New record: {}", record);
                                        try {
                                            /*TextMessage updateMsg = pluginBuilder.getAgentService().getDataPlaneService().createTextMessage();
                                            updateMsg.setText(gson.toJson(record));
                                            updateMsg.setStringProperty(BadgeCheckInStatics.SWIPE_RECORD_DATA_PLANE_IDENTIFIER_KEY,
                                                    BadgeCheckInStatics.getSiteSwipeRecordDataPlaneValue(siteName));
                                            pluginBuilder.getAgentService().getDataPlaneService().sendMessage(TopicType.AGENT, updateMsg);*/
                                            TextMessage updateMsg = pluginBuilder.getAgentService().getDataPlaneService()
                                                    .createTextMessage();
                                            updateMsg.setText(gson.toJson(record));
                                            updateMsg.setStringProperty(BadgeCheckInStatics.SWIPE_RECORD_DATA_PLANE_IDENTIFIER_KEY,
                                                    BadgeCheckInStatics.SWIPE_RECORD_DATA_PLANE_IDENTIFIER_VALUE);
                                            pluginBuilder.getAgentService().getDataPlaneService().sendMessage(TopicType.AGENT, updateMsg);
                                            updateMsg.setStringProperty(BadgeCheckInStatics.SWIPE_RECORD_DATA_PLANE_IDENTIFIER_KEY,
                                                    BadgeCheckInStatics.getSiteSwipeRecordDataPlaneValue(getSiteId()));
                                            pluginBuilder.getAgentService().getDataPlaneService().sendMessage(TopicType.AGENT, updateMsg);
                                        } catch (JMSException e) {
                                            logger.error("Failed to generate swipe message: {}, code: {}", e.getMessage(), e.getErrorCode());
                                            logger.trace("JMSException:\n" + ExceptionUtils.getStackTrace(e));
                                        }
                                        data = "";
                                    } else {
                                        data += character;
                                    }
                                }
                                break;
                        }
                    }
                }
                logger.info("Freeing resources");
                hidDevice.close();
                //hidServices.stop();
                //hidServices.shutdown();
            } /*catch (InterruptedException e) {
                logger.error("Interrupted exception: {}", e.getMessage());
                logger.trace("Interrupted exception:\n" + ExceptionUtils.getStackTrace(e));
            }*/ catch (NullPointerException e) {
                logger.error("Null pointer exception: {}", e.getMessage());
                logger.trace("Null pointer exception:\n" + ExceptionUtils.getStackTrace(e));
            } catch (HidException e) {
                logger.error("HID exception: {}", e.getMessage());
                logger.trace("HID exception:\n" + ExceptionUtils.getStackTrace(e));
            }
        }
    }

    private class CardReaderHeartBeatTask extends TimerTask {
        private PluginBuilder pluginBuilder;
        private CLogger logger;

        public CardReaderHeartBeatTask(PluginBuilder pluginBuilder) {
            this.pluginBuilder = pluginBuilder;
            this.logger = pluginBuilder.getLogger(this.getClass().getName(), CLogger.Level.Info);
        }

        @Override
        public void run() {
            try {
                TextMessage updateMsg = pluginBuilder.getAgentService().getDataPlaneService()
                        .createTextMessage();
                updateMsg.setText("alive");
                updateMsg.setStringProperty(BadgeCheckInStatics.STATION_HEARTBEAT_DATA_PLANE_IDENTIFIER_KEY,
                        siteId);
            } catch (JMSException e) {
                logger.error("Failed to create/send heartbeat message");
            }
        }
    }
}
