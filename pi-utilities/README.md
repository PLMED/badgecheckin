# Building a badge swipe endpoint using a Raspberry Pi 3 B+
To build a self-contained badge swipe endpoint, you'll require the following hardware:
* [Raspberry Pi 3 B+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/)
* [Pi Touchscreen/Case](https://www.amazon.com/Raspberry-320x480-Monitor-Raspbian-RetroPie/dp/B07N38B86S/)
* [Magnetic Card Reader](https://www.amazon.com/gp/product/B00E85TH9I/)
* \>= 32GB MicroSD Card
* MicroUSB Power Cord

## 1 - Assemble the components
The assembly mainly involves adding heatsinks and the display hat to the Raspberry Pi and inserting it into the
included housing. There should be instructions included with the product if order via Amazon that show how this is
accomplished.

## 2 - Install the operating environment
To initially provision the system, you will additionally need the following:
* An HDMI-compatible monitor or television
* An HDMI cable
* A USB keyboard and mouse

### 2.1 - Install the Raspian operating system

#### 2.1.1 - Download the image file
Browse to the Raspberry Pi Raspbian download [website](https://www.raspberrypi.org/downloads/raspbian/) and download
the latest image file with desktop only (not recommended software) to a system with an SD card reader.

#### 2.1.2 - Burn the image to the MicroSD card
Then use an image-writing software to write the Raspbian image to your MicroSD card, such as 
[balenaEtcher](https://www.balena.io/etcher/).

#### 2.1.3 - Boot the Pi and configure the system
Once the image is written and verified, insert the MicroSD card into the Raspberry Pi. The base operating system (OS)
does not have drivers for the touchscreen, so you will need to attach the Pi to the HDMI-compatible monitor for initial
configuration, as well as the keyboard and mouse. Upon first boot, the OS will resize the file-system to the size of 
the MicroSD card to fully utilize the space. After rebooting, you will be prompted to configure the initial user 
account. Please enable the SSH and VNC services during this step. The device will reboot once more and you'll be 
greeted with the desktop.

### 2.2 - Install the Touchscreen Drivers
As the root user (you can become the root user by using the `sudo su -` command) clone LCD-show GitHub 
[repository](https://github.com/goodtft/LCD-show) into the `/opt` directory using 
`cd /opt && git clone https://github.com/goodtft/LCD-show`. Once the repository is cloned, `cd` into the directory and
run the command `./MHS35-show` to install the drivers for the 3.5" LCD display and reboot the Pi. Once the system 
reboots, the LCD display will be active and the HDMI display will no longer function. Should you wish to re-enable the
HDMI-port as well, simply open a command line, browse to the `/opt/LCD-show` directory, and run the command 
`./LCD-hmdi`.

### 2.3 - Install OpenJDK 1.8 JDK
Open a terminal or SSH into the Pi and run the command `sudo apt install openjdk-8-jdk` to install the latest OpenJDK
Java 1.8 development environment.

### 2.4 - Install the latest Cresco components

#### 2.4.1 Install the Cresco agent
Using an administrative account, create the Cresco direcotry with  `sudo mkdir /opt/cresco` and install the included 
`agent-1.0-SNAPSHOT.jar` file in it.

#### 2.4.2 Install the Badge Agent specific plugins
Create the required Badge Agent directories with the command `sudo mkdir -p /opt/badge-agent/run_here`.

### 2.5 - Install the web frontend
The swipe device uses a local Nginx frontend page to display status messages from the Cresco backend to the user. 

#### 2.5.1 - Install web files
The [display-web](../display-web) directory contains the files needed by the Nginx server. All files in that directory
should be installed in the `/var/www/html` directory.

#### 2.5.2 - Install Nginx
To install Nginx use the command `sudo apt install nginx`. After the install completes, update the default 
configuration to match that of the included `default` file, test the configuration with `sudo nginx -t`. If there are
no errors, reload the configuration with `sudo nginx -s reload` or `sudo systemctl reload nginx`.

## 3 - Configuring the Cresco agent
**NOTE:** From here, this guide assumes you have the controller up and running on a server to which your Raspberry Pi 
can connect. If not, please set up the controller before continuing.

### 3.1 - Clone this repository
First, clone this repository into `/opt/badge-agent` as the root user. This should resemble the following:
`sudo su && cd /opt/badge-agent && git clone https://gitlab.com/PLMED/badgecheckin.git`.

### 3.2 - Copy Cresco configuration files
As the root user, copy the required configuration files into the `/opt/badge-agent` directory with the following
commands: `cp /opt/badge-agent/badgecheckin/pi-utilities/badge-agent.ini.example /opt/badge-agent/badge-agent.ini` and
`cp /opt/badge-agent/badgecheckin/pi-utilities/badge-agent-plugins.ini.example /opt/badge-agent/badge-agent-plugins.ini`.

### 3.3 - Modify the `/opt/badge-agent/badge-agent.ini` configuration file
You'll need do modify the `/opt/badge-agent/badge-agent.ini` using your preferred text editor as follows:
``` 
[general]
agentname=                      *Use the room identifier (e.g. MS-147)*
regionname=                     *Use the same regionname as the existing controller*
is_agent=true
enable_sysinfo=false
discovery_secret_agent=         *Use the same discovery_secret_agent as the existing controller* 
discovery_secret_region=        *Use the same discovery_secret_region as the existing controller*
discovery_secret_global=        *Use the same discovery_secret_global as the existing controller*
regional_controller_host=       *Use the IP address of the host machine of the existing controller*
plugin_config_file = /opt/badge-agent/badge-agent-plugins.ini
```

## 4 - Testing the current setup
First, connect the USB magnetic card reader to an available USB port on the Raspberry Pi. Then, as the root user, 
navigate to the run directory with `cd /opt/badge-agent/run_here`. From here, test the current Cresco configuration 
with `java -jar /opt/cresco/agent-1.0-SNAPSHOT.jar -f /opt/badge-agent/badge-agent.ini`. If everything is configured 
properly, you should see 

## 5 - Automating start-up and restart

### 5.1 - Update the following system files on the Raspberry Pi

#### `/etc/xdg/lxsession/LXDE-pi/autostart/desktop.conf`
Update with the contents of the included `desktop.conf` file to start the status display on load.

#### `/etc/X11/xorg.conf.d/99-calibration.conf`
Update with the contents of the included `99-calibration.conf` file to rotate the screen.

#### `/lib/systemd/system/badge-agent.service`
Install the service file `badge-agent.service` and run `sudo systemctl daemon-reload` and 
`sudo systemctl enable badge-agent.service`.