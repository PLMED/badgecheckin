'use strict';

let uuid4 = require(`uuid4`);

function User(username, password, firstName, lastName) {
    this.id = uuid4();
    this.username = username;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
}

User.prototype.checkPassword = function(password) {
    return this.password === password;
};

User.prototype.getName = function() {
    return `${this.firstName} ${this.lastName}`;
};

const admin = new User(`caylin`, `password`, `Caylin`, `Hickey`);

let users = {
    "caylin": admin
};

let user = admin;

const express = require('express'),
    mustacheExpress = require('mustache-express'),
    app = express(),
    port = process.env.PORT || 5000;

app.engine('mustache', mustacheExpress());
app.set('view engine', 'mustache');
app.set('views', __dirname + "/");
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use('/includes', express.static('includes'));
app.use('/css', express.static('css'));
app.use('/img', express.static('img'));
app.use('/js', express.static('js'));
app.use('/vendors', express.static('vendors'));

/**
 * Root Routes
 */
app.get('/', (req, res) => {
    if (user === undefined)
        res.redirect("/login");
    else {
        res.render('index', {
            page: {
                title: "Some Page"
            },
            user: {
                name: user.getName()
            }
        });
    }
});
app.get('/login', (req, res) => {
    res.render('login');
});
app.post('/login', (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    if (users.hasOwnProperty(username)) {
        const toCheck = users[username];
        if (toCheck.checkPassword(password)) {
            user = toCheck;
            res.redirect('/');
        } else {
            res.render('login', {
                error: `Invalid login credentials`
            });
        }
    } else {
        res.render('login', {
            error: `User "${username}" does not exist`
        });
    }
});
app.get('/logout', (req, res) => {
    user = undefined;
    res.redirect('/login');
});

app.use(function(req, res) {
    res.status(404).render('error', {
        errorNo: 404,
        errorMsg: 'Page not found'
    });
});

/**
 * Mock-Server Startup Section
 */
app.listen(port);
console.log(`Mock Cresco Server running at http://localhost:${port}/`);