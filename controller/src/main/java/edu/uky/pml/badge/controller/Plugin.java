package edu.uky.pml.badge.controller;

import edu.uky.pml.badge.controller.controllers.RootController;
import edu.uky.pml.badge.controller.filters.AuthenticationFilter;
import edu.uky.pml.badge.controller.services.SwipeService;
import edu.uky.pml.badge.controller.utilities.SessionFactoryManager;
import edu.uky.pml.badge.controller.utilities.SwipeListener;
import io.cresco.library.agent.AgentService;
import io.cresco.library.messaging.MsgEvent;
import io.cresco.library.plugin.Executor;
import io.cresco.library.plugin.PluginBuilder;
import io.cresco.library.plugin.PluginService;
import io.cresco.library.utilities.CLogger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.*;

import java.util.Map;

@Component(
        service = { PluginService.class },
        scope= ServiceScope.PROTOTYPE,
        configurationPolicy = ConfigurationPolicy.REQUIRE,
        servicefactory = true,
        property="dashboard=core",
        reference= { @Reference(name="io.cresco.library.agent.AgentService", service= AgentService.class) }
)
public class Plugin implements PluginService {
    public BundleContext context;
    public static PluginBuilder pluginBuilder;
    private Executor executor;
    private CLogger logger;
    private Map<String,Object> map;
    private Server jettyServer;
    private ServletHolder jerseyServlet;
    private SwipeListener swipeListener;

    @Activate
    void activate(BundleContext context, Map<String,Object> map) {
        this.context = context;
        this.map = map;
    }

    @Modified
    void modified(BundleContext context, Map<String,Object> map) {
        System.out.println("Modified Config Map PluginID:" + (String) map.get("pluginID"));
    }

    @Deactivate
    void deactivate(BundleContext context, Map<String,Object> map) {
        if(this.context != null) {
            this.context = null;
        }
        if(this.map != null) {
            this.map = null;
        }
    }

    @Override
    public boolean isActive() {
        return pluginBuilder.isActive();
    }

    @Override
    public void setIsActive(boolean isActive) {
        pluginBuilder.setIsActive(isActive);
    }

    @Override
    public boolean inMsg(MsgEvent incoming) {
        pluginBuilder.msgIn(incoming);
        return true;
    }

    @Override
    public boolean isStarted() {
        try {
            //this will be called twice due to JAX-RS-Connector
            if(pluginBuilder == null) {
                pluginBuilder = new PluginBuilder(this.getClass().getName(), context, map);
                this.logger = pluginBuilder.getLogger(Plugin.class.getName(), CLogger.Level.Info);
                this.executor = new ExecutorImpl(pluginBuilder);
                pluginBuilder.setExecutor(executor);

                while (!pluginBuilder.getAgentService().getAgentState().isActive()) {
                    logger.info("Plugin " + pluginBuilder.getPluginID() + " waiting on Agent Init");
                    //System.out.println("Plugin " + pluginBuilder.getPluginID() + " waiting on Agent Init");
                    Thread.sleep(1000);
                }

                swipeListener = new SwipeListener(pluginBuilder);
                swipeListener.start();

                SwipeService.startLogging(pluginBuilder);

                ResourceConfig rc = new ResourceConfig()
                        .register(AuthenticationFilter.class)
                        .register(RootController.class)
                        ;
                ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
                context.setContextPath("/");
                jettyServer = new Server(7116);
                jettyServer.setHandler(context);
                jerseyServlet = new ServletHolder(new org.glassfish.jersey.servlet.ServletContainer(rc));
                jerseyServlet.setInitOrder(0);
                context.addServlet(jerseyServlet, "/*");
                try {
                    jettyServer.start();
                } catch (Exception e) {
                    logger.error("Could not start embedded web server");
                    e.printStackTrace();
                }
                pluginBuilder.setIsActive(true);
            }
            return true;
        } catch(Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean isStopped() {
        if(swipeListener != null)
            swipeListener.stop();
        if(jettyServer != null) {
            if(!jettyServer.isStopped()) {
                try {
                    if(jerseyServlet != null) {
                        if(!jerseyServlet.isStopped()) {
                            logger.info("Stopping Jersey Servlet");
                            jerseyServlet.stop();
                            while(!jerseyServlet.isStopped()) {
                                Thread.sleep(100);
                            }
                            jerseyServlet = null;
                            logger.info("Jersey Servlet stopped");
                        }
                    }

                    logger.info("Stopping Jetty Server");
                    jettyServer.stop();
                    while(!jettyServer.isStopped()) {
                        Thread.sleep(100);
                    }
                    jettyServer = null;
                    logger.info("Jetty Server stopped");
                } catch (Exception ex) {
                    logger.error("embedded web server shutdown error : " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
        }
        SessionFactoryManager.close();
        if(pluginBuilder != null) {
            pluginBuilder.setExecutor(null);
            pluginBuilder.setIsActive(false);
        }
        return true;
    }

}
