package edu.uky.pml.badge.controller.utilities;

import com.google.gson.Gson;
import edu.uky.pml.badge.common.BadgeCheckInStatics;
import edu.uky.pml.badge.common.SwipeRecord;
import edu.uky.pml.badge.controller.services.SwipeService;
import edu.uky.pml.badge.identity.IDMLookup;
import edu.uky.pml.badge.identity.IDMLookupException;
import edu.uky.pml.badge.identity.UKIdentity;
import edu.uky.pml.badge.utilities.RollingFileAppender;
import io.cresco.library.data.TopicType;
import io.cresco.library.plugin.PluginBuilder;
import io.cresco.library.utilities.CLogger;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.io.IOException;
import java.nio.file.Paths;

public class SwipeListener {
    private PluginBuilder pluginBuilder;
    private CLogger logger;
    private RollingFileAppender backupFileAppender;

    //private String siteId;
    private String listenerId = null;
    private IDMLookup idmLookup;
    private String idmUserID;
    private String idmPassword;
    private String idmUrl;

    public SwipeListener(PluginBuilder pluginBuilder) {
        this.pluginBuilder = pluginBuilder;
        this.logger = pluginBuilder.getLogger(SwipeListener.class.getName(), CLogger.Level.Trace);
        //setSiteId(pluginBuilder.getConfig().getStringParam("site_id", "SiteA"));
        setIdmUserID(pluginBuilder.getConfig().getStringParam("idm_user_id"));
        setIdmPassword(pluginBuilder.getConfig().getStringParam("idm_password"));
        setIdmUrl(pluginBuilder.getConfig().getStringParam("idm_url"));
        idmLookup = new IDMLookup(idmUserID, idmPassword, idmUrl);
        try {
            backupFileAppender = new RollingFileAppender(Paths.get(pluginBuilder.getConfig().getStringParam("swipe_logs_path", "swipe-data/logs")));
        } catch (IllegalArgumentException e) {
            logger.error("Error creating swipe backup log utility: {}", e.getMessage());
        } catch (IOException e) {
            logger.error("Error creating swipe backup log utility: {}", e.getMessage());
            e.printStackTrace();
        }
    }

    public void start() {
        if (listenerId == null) {
            MessageListener ml = (Message msg) -> {
                try {
                    if (msg instanceof TextMessage) {
                        Gson gson = new Gson();
                        TextMessage textMessage = (TextMessage) msg;
                        SwipeRecord swipe = gson.fromJson(textMessage.getText(), SwipeRecord.class);
                        if (swipe.getId() != null) {
                            logger.info("Record@Controller: {} from {} ({}-{}-{}) at {}", swipe.getId(), swipe.getSite(),
                                    swipe.getCrescoRegion(), swipe.getCrescoAgent(), swipe.getCrescoPlugin(),
                                    swipe.getDateAsDate());
                            try {
                                logger.info("Using IDM to lookup: {}", swipe.getId());
                                UKIdentity ukId = idmLookup.testLookup(swipe.getId());
                                swipe.setLinkBlue(ukId.getLinkBlue());
                                swipe.setFullName(ukId.getFullName());
                            } catch (IDMLookupException e) {
                                e.printStackTrace();
                                swipe.setError(e.getMessage());
                            }
                            backupSwipe(swipe);
                            SwipeService.create(swipe);
                            TextMessage updateMsg = pluginBuilder.getAgentService().getDataPlaneService()
                                    .createTextMessage();
                            updateMsg.setText(gson.toJson(swipe));
                            updateMsg.setStringProperty(BadgeCheckInStatics.SWIPE_RESULT_DATA_PLANE_IDENTIFIER_KEY,
                                    BadgeCheckInStatics.getSiteSwipeResultDataPlaneValue(swipe.getSite()));
                            pluginBuilder.getAgentService().getDataPlaneService().sendMessage(TopicType.AGENT, updateMsg);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            };
            listenerId = pluginBuilder.getAgentService().getDataPlaneService().addMessageListener(
                    TopicType.AGENT,
                    ml,
                    String.format(
                            "%s='%s'",
                            BadgeCheckInStatics.SWIPE_RECORD_DATA_PLANE_IDENTIFIER_KEY,
                            BadgeCheckInStatics.SWIPE_RECORD_DATA_PLANE_IDENTIFIER_VALUE
                    )
            );
        }
    }

    public void stop() {
        if (listenerId != null) {
            pluginBuilder.getAgentService().getDataPlaneService().removeMessageListener(listenerId);
        }
    }

    private void backupSwipe(SwipeRecord swipeRecord) {
        if (backupFileAppender != null && swipeRecord != null)
            try {
                backupFileAppender.append(String.format("%s,%s,%s,%s,%s,%s",
                        swipeRecord.getDateAsDate(),
                        swipeRecord.getSite(),
                        swipeRecord.getId(),
                        (swipeRecord.getLinkBlue() != null) ? swipeRecord.getLinkBlue() : "",
                        (swipeRecord.getFullName() != null) ? swipeRecord.getFullName() : "",
                        (swipeRecord.getError() != null) ? swipeRecord.getError() : ""));
            } catch (IOException e) {
                logger.error("Failed to log swipe ({},{},{},{},{},{})",
                        swipeRecord.getDateAsDate(),
                        swipeRecord.getSite(),
                        swipeRecord.getId(),
                        (swipeRecord.getLinkBlue() != null) ? swipeRecord.getLinkBlue() : "",
                        (swipeRecord.getFullName() != null) ? swipeRecord.getFullName() : "",
                        (swipeRecord.getError() != null) ? swipeRecord.getError() : "");
            }
    }

    /*public String getSiteId() {
        return siteId;
    }
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }*/

    public String getIdmUserID() {
        return idmUserID;
    }
    public void setIdmUserID(String idmUserID) {
        this.idmUserID = idmUserID;
    }

    public String getIdmPassword() {
        return idmPassword;
    }
    public void setIdmPassword(String idmPassword) {
        this.idmPassword = idmPassword;
    }

    public String getIdmUrl() {
        return idmUrl;
    }
    public void setIdmUrl(String idmUrl) {
        this.idmUrl = idmUrl;
    }
}
