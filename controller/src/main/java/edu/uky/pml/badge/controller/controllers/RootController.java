package edu.uky.pml.badge.controller.controllers;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheException;
import com.github.mustachejava.MustacheFactory;
import com.google.gson.Gson;
import edu.uky.pml.badge.controller.Plugin;
import edu.uky.pml.badge.controller.filters.AuthenticationFilter;
import edu.uky.pml.badge.controller.models.LoginSession;
import edu.uky.pml.badge.controller.services.LoginSessionService;
import edu.uky.pml.badge.controller.services.SwipeService;
import io.cresco.library.plugin.PluginBuilder;
import io.cresco.library.utilities.CLogger;
import org.apache.commons.lang3.exception.ExceptionUtils;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.*;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.*;

@Path("/")
public class RootController {
    private PluginBuilder plugin;
    private CLogger logger;

    private static final String LOGIN_ERROR_COOKIE_NAME = "badgeSwipeLoginError";
    public static final String LOGIN_REDIRECT_COOKIE_NAME = "badgeSwipeLoginRedirect";

    public RootController() {
        if(plugin == null) {
            if(Plugin.pluginBuilder != null) {
                plugin = Plugin.pluginBuilder;
                logger = plugin.getLogger(RootController.class.getName(), CLogger.Level.Info);
            }
        }
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response index(@CookieParam(AuthenticationFilter.SESSION_COOKIE_NAME) String sessionID) {
        try {
            LoginSession loginSession = LoginSessionService.getByID(sessionID);
            MustacheFactory mf = new DefaultMustacheFactory();
            Mustache mustache = mf.compile("index.mustache");

            Map<String, Object> context = new HashMap<>();
            try {
                context.put("user", (loginSession != null) ? loginSession.getUsername() : "Unknown");
                context.put("section", "root");
                context.put("page", "index");
            } catch(Exception ex) {
                ex.printStackTrace();
            }

            Writer writer = new StringWriter();
            mustache.execute(writer, context);

            return Response.ok(writer.toString()).build();
        } catch (MustacheException e) {
            return Response.ok("Template Exception: " + e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ok("Server error: " + e.getMessage()).build();
        }
    }

    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    public Response dtSwipes(
            @CookieParam(AuthenticationFilter.SESSION_COOKIE_NAME) String sessionID,
            @QueryParam("draw") int draw,
            @QueryParam("order[0][column]") int sortColumn,
            @QueryParam("order[0][dir]") String order,
            @QueryParam("search[value]") String filter,
            @QueryParam("start") int start,
            @QueryParam("length") int length,
            @QueryParam("site") String site,
            @QueryParam("from") String from,
            @QueryParam("to") String to) {
        if (site.equals("all"))
            site = null;
        Map<String, Object> dataTable = new HashMap<>();
        try {
            String sortBy = "ts";
            switch (sortColumn) {
                case 0: sortBy = "ts"; break;
                case 1: sortBy = "site"; break;
                case 2: sortBy = "link_blue"; break;
                case 3: sortBy = "full_name"; break;
            }
            dataTable.put("draw", draw);
            dataTable.put("recordsTotal", SwipeService.getCount(site, from, to));
            dataTable.put("recordsFiltered", SwipeService.getFilteredCount(site, from, to, filter));
            List<Object> data = new ArrayList<>();
            List<Object[]> swipes = SwipeService.list(site, from, to, sortBy, order, filter, start, length);
            for (int i = 0; i < swipes.size(); i++) {
                Object[] swipe = swipes.get(i);
                Map<String, Object> dataEntry = new HashMap<>();
                dataEntry.put("DT_RowId", i);
                dataEntry.put("id", swipe[0]);
                dataEntry.put("ts", swipe[1]);
                dataEntry.put("site", swipe[2]);
                dataEntry.put("swipe", swipe[3]);
                dataEntry.put("ukID", swipe[4]);
                dataEntry.put("linkBlue", (swipe[5] != null) ? swipe[5] : " ");
                dataEntry.put("fullName", (swipe[6] != null) ? swipe[6] : " ");
                data.add(dataEntry);
            }
            dataTable.put("data", data);
        } catch (Exception e) {
            logger.error("list() Exception - {}:{}", e.getClass().getCanonicalName(), e.getMessage());
        }
        try {
            Gson gson = new Gson();
            return Response.ok(gson.toJson(dataTable), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("list() return exception:\n" + ExceptionUtils.getStackTrace(e));
            return Response.ok("{\"draw\":" + draw + ",\"recordsTotal\":0,\"recordsFiltered\":0,\"data\":[]}", MediaType.APPLICATION_JSON_TYPE).build();
        }
    }

    @GET
    @Path("download/{site}/{from}/{to}/{filter:.*}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response downloadCSV(
            @CookieParam(AuthenticationFilter.SESSION_COOKIE_NAME) String sessionID,
            @PathParam("site") String site,
            @PathParam("from") String from,
            @PathParam("to") String to,
            @PathParam("filter") String filter) {
        try {
            if (site.equals("all"))
                site = null;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm");
            String fileName = String.format("%sswipes_%s_%s.csv",
                    (site != null) ? site + "-" : "",
                    sdf.format(new Date(Long.valueOf(from))),
                    sdf.format(new Date(Long.valueOf(to))));
            SimpleDateFormat sdf_date_out = new SimpleDateFormat("yyyy-MM-dd");
            sdf_date_out.setTimeZone(TimeZone.getTimeZone("America/New_York"));
            SimpleDateFormat sdf_time_out = new SimpleDateFormat("HH:mm");
            sdf_time_out.setTimeZone(TimeZone.getTimeZone("America/New_York"));
            List<Object[]> swipes = SwipeService.download(site, from, to, filter);
            return Response.ok(new StreamingOutput() {
                @Override
                public void write(OutputStream os) throws IOException, WebApplicationException {
                    Writer writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write("date,time,timezone,site,ukid,linkblue,fullname\n");
                    for (Object[] swipe : swipes) {
                        try {
                            writer.write(String.format("%s,%s,%s,%s,%s,%s,%s\n",
                                    sdf_date_out.format(new Date(((BigInteger) swipe[0]).longValue())),
                                    sdf_time_out.format(new Date(((BigInteger) swipe[0]).longValue())),
                                    "EST",
                                    swipe[1],
                                    swipe[2],
                                    swipe[3],
                                    swipe[4]));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    writer.flush();
                }
            })
            .header("Content-type", "text/csv")
            .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"")
            .build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @PermitAll
    @GET
    @Path("swipes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listSwipes() {
        Gson gson = new Gson();
        try {
            Map<String, Object> ret = new HashMap<>();
            ret.put("swipes", SwipeService.all());
            return Response.ok(gson.toJson(ret)).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.serverError().build();
        }
    }

    @PermitAll
    @GET
    @Path("sites")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listSites() {
        Gson gson = new Gson();
        Map<String, Object> ret = new HashMap<>();
        try {
            ret.put("sites", SwipeService.getSites());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok(gson.toJson(ret)).build();
    }

    /*@PermitAll
    @GET
    @Path("/identity/login")
    public Response redirectToSSO() {
        try {

            Map<String, Object> context = new HashMap<>();
            MustacheFactory mf = new DefaultMustacheFactory();
            Mustache mustache = mf.compile("login_sso.mustache");

            Reader bReader = new InputStreamReader(
                    this.getClass().getResourceAsStream("/tsuyoi_auth0_com-metadata.xml"));
            // adapt TCCL
            Thread thread = Thread.currentThread();
            ClassLoader loader = thread.getContextClassLoader();
            thread.setContextClassLoader(InitializationService.class.getClassLoader());
            try {
                //InitializationService.initialize();
                BadgeSamlClient client = BadgeSamlClient.fromMetadata("urn:tsuyoi.auth0.com", "http://localhost:7116/identity/callback", bReader);
                context.put("SAMLRequest", client.getSamlRequest());
                //context.put("SAMLLogoutRequest", client.getSamlLogoutRequest("auth0|5d013eee2f4f1e0dc569d6e0"));
                context.put("SAMLLogoutURL", String.format("https://%s/v2/logout?client_id=%s&returnTo=%s",
                        "tsuyoi.auth0.com",
                        "UjW9Ss6sTK4WUqBAuC2VEkfx74nIK1hA",
                        "http://localhost:7116/identity/login"));
                context.put("SAMLurl", client.getIdentityProviderUrl());

                Writer writer = new StringWriter();
                mustache.execute(writer, context);

                return Response.ok(writer.toString()).build();
            } finally {
                // reset TCCL
                thread.setContextClassLoader(loader);
            }
        } catch (MustacheException e) {
            return Response.ok("Template Exception: " + e.getMessage()).build();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("{}", e.getMessage());
            return Response.serverError().build();
        }
    }*/

    /*@PermitAll
    @POST
    @Path("/identity/callback")
    public Response ssoCallback(@CookieParam(LOGIN_REDIRECT_COOKIE_NAME) String redirect,
                                @FormParam("SAMLResponse") String samlResponse) {
        try {
            Reader bReader = new BufferedReader(new InputStreamReader(
                    this.getClass().getResourceAsStream("/tsuyoi_auth0_com-metadata.xml")));

            // adapt TCCL
            Thread thread = Thread.currentThread();
            ClassLoader loader = thread.getContextClassLoader();
            thread.setContextClassLoader(InitializationService.class.getClassLoader());
            String authenticatedUser = "unknown";
            try {
                BadgeSamlClient client = BadgeSamlClient.fromMetadata("urn:tsuyoi.auth0.com", "http://localhost:7116/identity/callback", bReader);
                SamlResponse response = client.decodeAndValidateSamlResponse(samlResponse);
                authenticatedUser = response.getNameID();
                Assertion assertion = response.getAssertion();
                for (AttributeStatement attributeStatement : assertion.getAttributeStatements()) {
                    for (Attribute attribute : attributeStatement.getAttributes()) {
                        //logger.info("Attribute: {}", attribute.getName());
                        for (XMLObject attributeValue : attribute.getAttributeValues()) {
                            try {
                                String value = ((XSString) attributeValue).getValue();
                                //logger.info("\t Value: {}", value);
                                if (attribute.getName().equals("http://schemas.auth0.com/nickname"))
                                    authenticatedUser = value;
                            } catch (Exception e) {

                            }
                        }
                    }
                }
            } finally {
                // reset TCCL
                thread.setContextClassLoader(loader);
            }
            LoginSession loginSession = LoginSessionService.create(authenticatedUser.trim(), false);
            return Response.seeOther(new URI(redirect))
                    .cookie(new NewCookie(AuthenticationFilter.SESSION_COOKIE_NAME, loginSession.getId(), "/", null, null, 60 * 60 * 24 * 365 * 10, false))
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("{}", e.getMessage());
            return Response.serverError().build();
        }
    }*/

    /*@PermitAll
    @GET
    @Path("/identity/logout")
    public Response ssoLogout(@CookieParam(AuthenticationFilter.SESSION_COOKIE_NAME) String sessionID) {
        try {
            LoginSessionService.delete(sessionID);
            NewCookie deleteSession = new NewCookie(AuthenticationFilter.SESSION_COOKIE_NAME, null, null, null, null, 0, false);
            return Response.seeOther(new URI(String.format("https://%s/v2/logout?client_id=%s&returnTo=%s",
                    "tsuyoi.auth0.com",
                    "UjW9Ss6sTK4WUqBAuC2VEkfx74nIK1hA",
                    "http://localhost:7116/identity/login"))).cookie(deleteSession).build();
        } catch (URISyntaxException e) {
            return Response.serverError().build();
        }
    }*/

    @PermitAll
    @GET
    @Path("/login")
    @Produces(MediaType.TEXT_HTML)
    public Response getLogin(@CookieParam(LOGIN_REDIRECT_COOKIE_NAME) String redirect,
                             @CookieParam(LOGIN_ERROR_COOKIE_NAME) String error) {
        try {
            MustacheFactory mf = new DefaultMustacheFactory();
            Mustache mustache = mf.compile("login.mustache");

            Map<String, Object> context = new HashMap<>();
            if (redirect != null)
                context.put("redirect", redirect);
            else
                context.put("redirect", "/");
            if (error != null)
                context.put("error", error);

            NewCookie deleteRedirect = new NewCookie(LOGIN_REDIRECT_COOKIE_NAME, null, null, null, null, 0, false);
            NewCookie deleteError = new NewCookie(LOGIN_ERROR_COOKIE_NAME, null, null, null, null, 0, false);

            Writer writer = new StringWriter();
            mustache.execute(writer, context);

            return Response.ok(writer.toString()).cookie(deleteRedirect, deleteError).build();
        } catch (MustacheException e) {
            return Response.ok("Template Exception: " + e.getMessage()).build();
        } catch (Exception e) {
            logger.error("{}", e.getMessage());
            return Response.serverError().build();
        }
    }

    @PermitAll
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response postLogin(@FormParam("username") String username,
                              @FormParam("password") String password,
                              @FormParam("rememberMe") Boolean rememberMe,
                              @FormParam("redirect") String redirect) {
        try {
            if (plugin == null) {
                if (username == null || username.equals("") || !username.toLowerCase().trim().equals("admin") ||
                        password == null || password.equals("") || !password.toLowerCase().trim().equals("cresco")) {
                    NewCookie errorCookie = new NewCookie(LOGIN_ERROR_COOKIE_NAME, "Invalid Username or Password!", null, null, null, 60 * 60, false);
                    return Response.seeOther(new URI("/login")).cookie(errorCookie).build();
                }
            } else {
                if (username == null || username.equals("") || !username.toLowerCase().trim().equals(plugin.getConfig().getStringParam("username", "admin").toLowerCase().trim()) ||
                        password == null || password.equals("") || !password.toLowerCase().trim().equals(plugin.getConfig().getStringParam("password", "cresco").toLowerCase().trim())) {
                    NewCookie errorCookie = new NewCookie(LOGIN_ERROR_COOKIE_NAME, "Invalid Username or Password!", null, null, null, 60 * 60, false);
                    return Response.seeOther(new URI("/login")).cookie(errorCookie).build();
                }
            }
            try {
                LoginSession loginSession = LoginSessionService.create(username.trim(), rememberMe != null);
                return Response.seeOther(new URI(redirect))
                        .cookie(new NewCookie(AuthenticationFilter.SESSION_COOKIE_NAME, loginSession.getId(), null, null, null, 60 * 60 * 24 * 365 * 10, false))
                        .build();
            } catch (Exception e) {
                e.printStackTrace();
                return Response.serverError().build();
            }
        } catch (URISyntaxException e) {
            return Response.serverError().build();
        }
    }

    @PermitAll
    @GET
    @Path("/logout")
    public Response getLogout(@CookieParam(AuthenticationFilter.SESSION_COOKIE_NAME) String sessionID) {
        try {
            LoginSessionService.delete(sessionID);
            NewCookie deleteSession = new NewCookie(AuthenticationFilter.SESSION_COOKIE_NAME, null, null, null, null, 0, false);
            return Response.seeOther(new URI("/login")).cookie(deleteSession).build();
        } catch (URISyntaxException e) {
            return Response.serverError().build();
        }
    }

    @PermitAll
    @GET
    @Path("/includes/{subResources:.*}")
    @Produces(MediaType.TEXT_HTML)
    public Response getIncludes(@PathParam("subResources") String subResources) {
        return getSubResourceStream("/includes/" + subResources);
    }

    @PermitAll
    @GET
    @Path("/css/{subResources:.*}")
    @Produces(MediaType.TEXT_HTML)
    public Response getCSS(@PathParam("subResources") String subResources) {
        return getSubResourceStream("/css/" + subResources);
    }

    @PermitAll
    @GET
    @Path("/js/{subResources:.*}")
    @Produces(MediaType.TEXT_HTML)
    public Response getJS(@PathParam("subResources") String subResources) {
        return getSubResourceStream("/js/" + subResources);
    }


    @PermitAll
    @GET
    @Path("/img/{subResources:.*}")
    @Produces(MediaType.TEXT_HTML)
    public Response getImg(@PathParam("subResources") String subResources) {
        return getSubResourceStream("/img/" + subResources);
    }

    @PermitAll
    @GET
    @Path("/vendors/{subResources:.*}")
    @Produces(MediaType.TEXT_HTML)
    public Response getVendors(@PathParam("subResources") String subResources) {
        return getSubResourceStream("/vendors/" + subResources);
    }

    private Response getSubResourceStream(String subResources) {
        try {
            InputStream in = getClass().getResourceAsStream(subResources);
            if (in == null) {
                System.out.println("NOT FOUND!");
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            return Response.ok(in, mediaType(subResources)).build();
        } catch(Exception ex) {
            System.out.println(ex.toString());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    private String mediaType(String file) {
        int dot = file.lastIndexOf(".");
        if (dot == -1) return MediaType.TEXT_PLAIN;
        String ext = file.substring(dot + 1).toLowerCase();
        switch (ext) {
            case "png":
                return "image/png";
            case "gif":
                return "image/gif";
            case "json":
                return MediaType.APPLICATION_JSON;
            case "js":
                return "text/javascript";
            case "css":
                return "text/css";
            case "svg":
                return "image/svg+xml";
            case "html":
                return MediaType.TEXT_HTML;
            case "txt":
                return MediaType.TEXT_PLAIN;
            case "jpg":
            case "jpeg":
                return "image/jpg";
            case "jar":
                return "application/java-archive";
            default:
                return MediaType.TEXT_PLAIN;
        }
    }

}