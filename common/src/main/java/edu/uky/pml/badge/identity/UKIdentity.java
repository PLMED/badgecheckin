package edu.uky.pml.badge.identity;

import java.util.List;

public class UKIdentity {
    public String Id;
    public String SponsorId;
    public String zUKID;
    public String CreatedTS;
    public String ModifiedTS;
    public String ExpireTS;
    public String DateOfBirth;
    public String SSN;
    public String linkblue;
    public Boolean isHuman;
    public String Notes;
    public Boolean Duplicate;
    public String Sponsor;
    public List<Name> Names;
    public List<Role> Roles;
    public List<Attribute> Attributes;

    public String getLinkBlue() {
        return linkblue;
    }

    public String getFullName() {
        if (Names.size() < 1)
            return null;
        Name name = Names.get(0);
        return String.format("%s %s", name.FirstName, name.LastName);
    }

    public class Name {
        public String Id;
        public String IdentityId;
        public String SystemId;
        public String CreatedTS;
        public String ModifiedTS;
        public String System;
        public String FirstName;
        public String MiddleName;
        public String LastName;
        public String Prefix;
        public String Suffix;
        public String NickName;
        public String Notes;
        public Boolean IsCurrent;
    }

    public class Role {
        public String Id;
        public String Name;
        public String Description;
        public Boolean Active;
        public List<TechnicalRole> TechnicalRoles;

        public class TechnicalRole {
            public String Id;
            public String FriendlyName;
            public String Description;
            public String Name;
            public Boolean Active;
            public String System;
            public String Type;
        }
    }

    public class Attribute {
        public String Name;
        public String Value;
        public String System;
    }
}
