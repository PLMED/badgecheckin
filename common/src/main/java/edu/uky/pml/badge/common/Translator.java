package edu.uky.pml.badge.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class Translator {
    private static Logger logger = LoggerFactory.getLogger(Translator.class);
    private static final Map<String, String> SpecialMap = new HashMap<String, String>() {{
        put( "8", "E");
        put("34", "%");
        put("46", "+");
        put("56", "?");
    }};
    private static final Map<String, String> CharacterMap = new HashMap<String, String>() {{
        put("30", "1");
        put("31", "2");
        put("32", "3");
        put("33", "4");
        put("34", "5");
        put("35", "6");
        put("36", "7");
        put("37", "8");
        put("38", "9");
        put("39", "0");
        put("46", "=");
        put("51", ";");
        put("88", "\n");
    }};

    public static String translate(byte[] toTranslate) {
        logger.trace("translate({})", toTranslate);
        if (toTranslate == null || toTranslate.length < 3)
            return null;
        String specialFlag = Byte.toString(toTranslate[0]);
        logger.debug("specialFlag: {}", specialFlag);
        String characterFlag = Byte.toString(toTranslate[2]);
        logger.debug("characterFlag: {}", characterFlag);
        if (!characterFlag.equals("0")) {
            String character = null;
            if (specialFlag.equals("2")) {
                if (SpecialMap.containsKey(characterFlag)) {
                    character = SpecialMap.get(characterFlag);
                } else {
                    logger.error("Failed to find special character: {}", characterFlag);
                }
            } else if (specialFlag.equals("0")) {
                if (CharacterMap.containsKey(characterFlag)) {
                    character = CharacterMap.get(characterFlag);
                } else {
                    logger.error("Failed to find regular character: {}", characterFlag);
                }
            } else {
                logger.error("Found erroneous special flag: {}", specialFlag);
            }
            return character;
        } else {
            logger.trace("characterFlag is '0'");
            return null;
        }
    }
}
