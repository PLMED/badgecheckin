package edu.uky.pml.badge.common;

@SuppressWarnings("WeakerAccess")
public class BadgeCheckInStatics {
    public static final String STATION_HEARTBEAT_DATA_PLANE_IDENTIFIER_KEY = "ukhcBadgeStation";
    public static final String SWIPE_RECORD_DATA_PLANE_IDENTIFIER_KEY = "cardSwipe";
    public static final String SWIPE_RECORD_DATA_PLANE_IDENTIFIER_VALUE = "ukhcBadgeSwipe";
    public static final String SWIPE_RESULT_DATA_PLANE_IDENTIFIER_KEY = "cardSwipeResult";
    public static final String SWIPE_RESULT_DATA_PLANE_IDENTIFIER_VALUE = "ukhcBadgeSwipeResult";

    public static String getSiteSwipeRecordDataPlaneValue(String site) {
        return String.format("%s-%s", SWIPE_RECORD_DATA_PLANE_IDENTIFIER_VALUE, site);
    }

    public static String getSiteSwipeResultDataPlaneValue(String site) {
        return String.format("%s-%s", SWIPE_RESULT_DATA_PLANE_IDENTIFIER_VALUE, site);
    }
}
