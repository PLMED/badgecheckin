// Notification Modal
var notificationModal = $('#notificationModal');
var notificationModalTitle = $('#notificationModalLabel')
var notificationModalBody = $('#notificationModalBody');
function showNotification(title, msg) {
    notificationModalTitle.html(title);
    notificationModalBody.html('<h6 class="text-dark">' + msg + '</h6>');
    notificationModal.modal('show');
}

// Error Modal
var errorModal = $('#errorModal');
var errorModalBody = $('#errorModalBody');
function showError(msg) {
    errorModalBody.html('<h6 class="text-danger">' + msg + '</h6>');
    errorModal.modal('show');
}