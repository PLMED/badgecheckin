<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" />
    <title>Pathology Attendence Tracking System</title>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/moment.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/dataTables.bootstrap4.min.css">
    <link type="text/css" rel="stylesheet" href="css/responsive.bootstrap4.min.css">
    <link type="text/css" rel="stylesheet" href="css/buttons.bootstrap4.min.css">
    <link type="text/css" rel="stylesheet" href="css/daterangepicker.css">
    <link type="text/css" rel="stylesheet" href="css/global.css">
</head>
<body>
<nav class="navbar navbar-dark sticky-top navbar-expand-lg flex-md-nowrap p-0">
    <a class="navbar-brand col-2 col-sm-2 col-md-2 mr-0" href="/">Pathology Attendence<br>Tracking System</a>
    <ul class="navbar-nav mr-auto">
    </ul>
<?php if (isset($user) && !is_null($user)) : ?>
    <form class="form-inline my-2 my-lg-0 mr-1">
        <span class="responsive-text" id="login-user"><?php echo $user->getLinkblue(); ?></span>
        <a class="btn btn-sm btn-primary my-2 my-sm-0 ml-1" href="/logout">Logout</a>
    </form>
<?php endif; ?>
</nav>
<div class="container-fluid">
    <div class="row min-vh-100 flex-column flex-md-row">
<?php include_once __DIR__ . '/_menu.php'; ?>
        <main role="main" class="col bg-faded py-3 flex-grow-1">
