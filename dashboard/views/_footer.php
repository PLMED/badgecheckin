        </main>
    </div>
</div>
<script src="js/jquery.inputMask.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/feather.min.js"></script>
<script src="js/jquery.timeago.js"></script>
<script src="js/uuid4.js"></script>
<script src="js/daterangepicker.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/dataTables.buttons.min.js"></script>
<script src="js/buttons.bootstrap4.min.js"></script>
<script src="js/buttons.colVis.min.js"></script>
<script src="js/modals.js"></script>
<script type="text/javascript">
    $(function() {
        feather.replace();
    });
</script>
</body>
</html>