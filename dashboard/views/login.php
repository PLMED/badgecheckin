<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="favicon.ico" type="image/vnd.microsoft.icon" />
    <title>Pathology Attendence Tracking System - Login</title>
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/login.css">
</head>
<body class="text-center">
<form class="form-signin" id="loginForm" action="/login" method="post">
    <h1 class="h3 mb-4 font-weight-normal logo"><a href="/">Pathology Attendence Tracking System</a></h1>
    <h1 class="h5 mb-3 font-weight-normal text-light">Please sign in</h1>
    <h1 class="h5 mb-3 text-secondary" id="login-message">
        <?php
            if (isset($_SESSION['LOGIN_ERROR']) && !is_null($_SESSION['LOGIN_ERROR'])) {
                echo $_SESSION['LOGIN_ERROR'];
                unset($_SESSION['LOGIN_ERROR']);
            }
        ?>
    </h1>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="text" name="user" id="inputEmail" class="form-control mb-2" placeholder="Linkblue" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
    <!--<label for="remember" class="sr-only">Remember me</label>
    <input class="form-control" type="checkbox" name="remember" id="remember" value="remember-me">-->
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <p class="mt-5 mb-3 text-light"><a class="text-light" href="https://ukhealthcare.uky.edu/" target="_blank">UK Healthcare</a> &copy; 2020</p>
</form>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/feather.min.js"></script>
</body>
</html>