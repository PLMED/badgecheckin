<?php
    include_once __DIR__ . '/_header.php';
?>
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <span class="h2">
            Attendence Badge Swipes
        </span>
        <button class="btn btn-success float-right" onclick="downloadCSV();">
            <span class="mr-1" data-feather="download"></span> Download (.csv)
        </button>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group row">
                <div class="col-lg-4 mb-2">
                    <label class="sr-only" for="swipes-site">Site</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Site:</span>
                        </div>
                        <select id="swipes-site" class="form-control">
                            <option value="all" selected>All</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-8 mb-2">
                    <div id="swipesDateRange" class="input-group" style="cursor: pointer;">
                        <div class="input-group-prepend text-center">
                            <div class="input-group-text"><span class="mr-2" data-feather="calendar"></span> Dates:</div>
                        </div>
                        <span class="form-control text-center"></span>
                    </div>
                </div>
            </div>
            <div id="swipesOverlay" style="display: none;" class="dataTables_overlay">
                <div class="dataTables_overlay_content">
                    <i class="fa fa-server"></i> Connection to server lost
                </div>
            </div>
            <table id="swipes" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th style="text-align: center;">Time</th>
                    <th style="text-align: center;">Site</th>
                    <th style="text-align: center;">Swipe</th>
                    <th style="text-align: center;">UKID</th>
                    <th style="text-align: center;">Linkblue</th>
                    <th style="text-align: center;">Full Name</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                    <th style="text-align: center;">Time</th>
                    <th style="text-align: center;">Site</th>
                    <th style="text-align: center;">Swipe</th>
                    <th style="text-align: center;">UKID</th>
                    <th style="text-align: center;">Linkblue</th>
                    <th style="text-align: center;">Full Name</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        var UPDATE_FREQUENCY = 5000;

        var swipesFilter;
        var swipes;
        var swipesFrom = moment().startOf('day');
        var swipesTo = moment().add(1, 'days').startOf('day');
        var timer = 0;
        var refreshSwipes = true
        var swipeTimeAgo = true;

        var swipesSite = $('#swipes-site');
        var swipesTable = $('#swipes');
        var swipesTableBody = $('#swipes tbody');
        var swipesDataTable = null;
        var swipesDateRange = $('#swipesDateRange');
        var swipesDateRangeSpan = $('#swipesDateRange span');
        var swipesOverlay = $('#swipesOverlay');

        function updateSites() {
            $.getJSON('/sites', function(data) {
                var selected = swipesSite.val();
                var options = '<option value="all"';
                if (selected === 'all') { options += 'selected'; }
                options += '>All</option>';
                $.each(data.sites, function(i, v) {
                    options += '<option value="' + v + '">' + v + '</option>';
                });
                swipesSite.html(options);
            });
        }

        swipesSite.change(function() {
            updateList();
        });

        function updateList() {
            if (refreshSwipes && !document.hidden)
                swipesDataTable.ajax.reload( null, false );
        }

        function downloadCSV() {
            var href = '/download/' + swipesSite.val() + '/' + swipesFrom.unix()*1000 + '/' + swipesTo.unix()*1000;
            if (swipesFilter.val() !== '' && swipesFilter.val() !== undefined)
                href += '/' + swipesFilter.val()
            document.location.href = href;
        }

        $(function() {
            updateSites();
            swipesDataTable = swipesTable.DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'pageLength', 'colvis',
                    {
                        text: 'Show Timestamps',
                        action: function (e/*, dt, node, config*/) {
                            if (swipeTimeAgo) {
                                swipeTimeAgo = false;
                                e.target.innerHTML = '<span>Show Relative Times</span>'
                            } else {
                                swipeTimeAgo = true;
                                e.target.innerHTML = '<span>Show Timestamps</span>'
                            }
                            updateList();
                        }
                    }
                ],
                order: [[0, "desc"]],
                pagingType: "full_numbers",
                responsive: true,
                processing: false,
                serverSide: true,
                stateSave: true,
                ajax: {
                    url: "/swipes",
                    type: "GET",
                    dataType: "JSON",
                    data: function(data) {
                        data.site = swipesSite.val();
                        data.from = swipesFrom.unix()*1000;
                        data.to = swipesTo.unix()*1000;
                    },
                    async: true,
                    error: function (/*xhr, ajaxOptions, thrownError*/) {
                        swipesOverlay.css("display", "");
                    },
                    complete: function (xhr/*, ajaxOptions, thrownError*/) {
                        if (xhr.readyState === 4) {
                            swipesOverlay.css("display", "none");
                        }
                    }
                },
                language: {
                    emptyTable: "No swipes have been recorded",
                    loadingRecords: "Loading records",
                    processing: "Connection to server lost..."
                },
                columnDefs: [
                    {
                        className: "dt-center",
                        targets: [1, 2, 3, 4, 5]
                    }, {
                        orderable: false,
                        targets: [2, 3]
                    }, {
                        visible: false,
                        targets: [2, 3]
                    }
                ],
                columns: [
                    {
                        data: null,
                        render: function ( data, type/*, row, meta*/ ) {
                            var ts = data.ts;
                            if (type === 'display' || type === 'filter') {
                                if (swipeTimeAgo)
                                    return "<time-ago class='timeago' datetime='" + moment(parseInt(ts)).toISOString() +
                                        "' title='" + moment(parseInt(ts)).format('MMMM Do YYYY, h:mm A zz') + "'>" +
                                        jQuery.timeago(new Date(parseInt(ts))) + "</time-ago>";
                                return moment(parseInt(ts)).format('MMMM Do, YYYY h:mm A zz');
                            } else {
                                return data;
                            }
                        }
                    }, {
                        data: "site"
                    },{
                        data: "swipe"
                    },{
                        data: "ukid"
                    },{
                        data: "linkblue"
                    },{
                        data: "fullname"
                    }
                ]
            });
            swipesFilter = $('#swipes_filter input');
            timer = setInterval(updateList, UPDATE_FREQUENCY);
            swipesDateRangeSpan.html( swipesFrom.format('MMM DD YYYY h:mm A') + ' - ' +
                swipesTo.format('MMM DD YYYY h:mm A'));
            swipesDateRange.daterangepicker(
                {
                    ranges: {
                        'Today': [moment().startOf('day'), moment().add(1, 'days').startOf('day')],
                        'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().startOf('day')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().utc().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'),
                            moment().subtract(1, 'month').endOf('month')],
                        'Last Year': [moment().subtract(1, 'year'), moment()]
                    },
                    startDate: moment().startOf('day'),
                    endDate: moment().add(1, 'days').startOf('day'),
                    timePicker: true,
                    timePicker12Hour: true,
                    timePickerIncrement: 5,
                    timeZone: "EST"
                },
                function(start, end) {
                    swipesFrom = start;
                    swipesTo = end;
                    swipesDateRangeSpan.html(
                        swipesFrom.format('MMM DD YYYY h:mm A') + ' - ' + swipesTo.format('MMM DD YYYY h:mm A')
                    );
                    updateList();
                }
            );
        });
    </script>
<?php
    include_once __DIR__ . '/_footer.php';