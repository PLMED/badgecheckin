<aside class="col-12 col-md-2 p-0 bg-light flex-shrink-1">
    <nav class="navbar navbar-short navbar-expand navbar-light bg-light flex-md-column flex-row align-items-start py-2">
        <div class="collapse navbar-collapse ">
            <ul class="flex-md-column flex-row navbar-nav w-100 justify-content-between">
                <li class="nav-item d-none d-md-inline">
                    <span class="nav-link pl-0 text-nowrap">Badge Swipes</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="View Current Rack">
                        <span data-feather="grid"></span>
                        <span class="d-none d-md-inline">View Swipes</span>
                    </a>
                </li>
                <!--<li class="nav-item">
                    <a class="nav-link" href="/search" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Search Swipe Database">
                        <span data-feather="search"></span>
                        <span class="d-none d-md-inline">Search Swipes</span>
                    </a>
                </li>-->
            </ul>
        </div>
    </nav>
</aside>