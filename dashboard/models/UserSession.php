<?php
    require_once __DIR__ . '/../utilities/db.php';

    class UserSession implements JsonSerializable {
        protected $id;
        protected $user_id;
        protected $last_seen;
        protected $remember_me;

        public function __construct() { }

        public static function create($id, $user_id, $remember_me) {
            $instance = new self();
            $instance->setId($id);
            $instance->setUserId($user_id);
            $instance->setRememberMe($remember_me);
            $instance = $instance->save();
            return $instance;
        }

        public static function all() {
            $sessions = [];
            $query = "SELECT * FROM [sessions] s";
            $stmt = DB::run($query);
            while ($row = $stmt->fetch(PDO::FETCH_LAZY))
                array_push($sessions, UserSession::withRow($row));
            return $sessions;
        }

        public static function withID($id) {
            try {
                $instance = new self();
                $instance->loadByID($id);
                if (!is_null($instance) && !is_null($instance->last_seen)) {
                    global $config;
                    $timeout = (isset($config['sessions']) &&
                        isset($config['sessions']['timeout']) &&
                        is_int($config['sessions']['timeout']))
                        ? $config['sessions']['timeout'] : 7200;
                    $now = new DateTime();
                    if (($now->getTimestamp() - $instance->last_seen->getTimestamp()) > $timeout) {
                        self::delete($instance->id);
                        $instance = null;
                    }
                }
                return $instance;
            } catch (PDOException $e) {
                return null;
            }
        }

        public static function withRow($row) {
            $instance = new self();
            $instance->fill($row);
            return $instance;
        }

        public static function delete($id) {
            $delete = "DELETE FROM [sessions] WHERE id = :id";
            DB::run($delete, ['id' => $id]);
        }

        protected function loadByID($id) {
            $stmt = DB::run(
                "SELECT * FROM [sessions] s WHERE s.id = :id ORDER BY s.id OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY",
                ['id' => $id]
            );
            $row = $stmt->fetch(PDO::FETCH_LAZY);
            if ($row <> null) {
                $this->fill($row);
                $this->seen();
            } else
                throw new PDOException("Session with id [{$id}] not found");
        }

        protected function fill($row) {
            $this->id = $row['id'];
            $this->user_id = $row['user_id'];
            $this->last_seen = new DateTime($row['last_seen']);
            $this->remember_me = $row['remember_me'];
        }

        protected function save() {
            $exists = UserSession::withID($this->getId());
            if (is_null($exists)) {
                $insert = "INSERT INTO [sessions] (id, user_id, remember_me) VALUES (?,?,?)";
                DB::run($insert, [$this->getId(), $this->getUserId(), $this->getRememberMe()]);
            } else {
                $update = "UPDATE [sessions] SET user_id=?, last_seen=GETDATE(), remember_me=? WHERE id=?";
                DB::run($update, [$this->getUserId(), $this->getRememberMe(), $this->getId()]);
            }
            return UserSession::withID($this->getId());
        }

        protected function seen() {
            $update = "UPDATE [sessions] SET last_seen=GETDATE() WHERE id=?";
            DB::run($update, [$this->getId()]);
        }

        /**
         * @return mixed
         */
        public function getId() {
            return $this->id;
        }
        /**
         * @param mixed $id
         */
        public function setId($id): void {
            $this->id = $id;
        }

        /**
         * @return mixed
         */
        public function getUserId() {
            return $this->user_id;
        }
        /**
         * @param mixed $user_id
         */
        public function setUserId($user_id): void {
            $this->user_id = $user_id;
        }

        /**
         * @return DateTime
         */
        public function getLastSeen() {
            return $this->last_seen;
        }

        /**
         * @return mixed
         */
        public function getRememberMe() {
            return $this->remember_me;
        }
        /**
         * @param mixed $remember_me
         */
        public function setRememberMe($remember_me): void {
            $this->remember_me = $remember_me;
        }

        /**
         * @return array|mixed
         */
        public function jsonSerialize() {
            return [
                'id'            => $this->getId(),
                'user_id'       => $this->getUserId(),
                'last_seen'     => $this->getLastSeen()->getTimestamp(),
                'remember_me'   => $this->getRememberMe(),
            ];
        }
    }