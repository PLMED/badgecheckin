<?php
    require_once __DIR__ . '/../utilities/db.php';
    require_once __DIR__ . '/../utilities/UUID.php';

    class User implements JsonSerializable {
        protected $id;
        protected $linkblue;
        protected $admin;

        public function __construct() {
            $this->id = UUID::v4();
        }

        public static function create($linkblue, $admin) {
            $instance = new self();
            $instance->setLinkblue($linkblue);
            $instance->setAdmin($admin);
            $instance = $instance->save();
            return $instance;
        }

        public static function all() {
            $users = [];
            $query = "SELECT * FROM [users] u";
            $stmt = DB::run($query);
            while ($row = $stmt->fetch(PDO::FETCH_LAZY))
                array_push($users, User::withRow($row));
            return $users;
        }

        public static function withId( $id ) {
            try {
                $instance = new self();
                $instance->loadById($id);
                return $instance;
            } catch (PDOException $e) {
                return null;
            }
        }

        public static function withLinkblue( $linkblue ) {
            try {
                $instance = new self();
                $instance->loadByLinkblue($linkblue);
                return $instance;
            } catch (PDOException $e) {
                return null;
            }
        }

        public static function withRow( $row ) {
            $instance = new self();
            $instance->fill( $row );
            return $instance;
        }

        protected function loadById( $id ) {
            $stmt = DB::run(
                "SELECT * FROM [users] u WHERE u.id = :id ORDER BY u.id OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY",
                ['id' => $id]
            );
            $row = $stmt->fetch(PDO::FETCH_LAZY);
            if ($row <> null)
                $this->fill( $row );
            else
                throw new PDOException("User with id [{$id}] not found");
        }

        protected function loadByLinkblue( $linkblue ) {
            $stmt = DB::run(
                "SELECT * FROM [users] u WHERE u.linkblue = :linkblue ORDER BY u.id OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY",
                ['linkblue' => $linkblue]
            );
            $row = $stmt->fetch(PDO::FETCH_LAZY);
            if ($row <> null)
                $this->fill( $row );
            else
                throw new PDOException("User with linkblue [{$linkblue}] not found");
        }

        protected function fill( $row ) {
            $this->id = $row['id'];
            $this->linkblue = $row['linkblue'];
            $this->admin = $row['admin'];
        }

        protected function save() {
            $exists = User::withId($this->getId());
            if (is_null($exists)) {
                $insert = "INSERT INTO [users] (id, linkblue, admin) VALUES (?,?,?)";
                DB::run($insert, [$this->getId(), $this->getLinkblue(), $this->isAdmin()]);
            } else {
                $update = "UPDATE [users] SET linkblue=?, admin=? WHERE id=?";
                DB::run($update, [$this->getLinkblue(), $this->isAdmin(), $this->getId()]);
            }
            return User::withId($this->getId());
        }

        /**
         * @return string
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * @return mixed
         */
        public function getLinkblue()
        {
            return $this->linkblue;
        }

        /**
         * @param mixed $linkblue
         */
        public function setLinkblue($linkblue): void
        {
            $this->linkblue = $linkblue;
        }

        /**
         * @return bool
         */
        public function isAdmin(): bool
        {
            return $this->admin;
        }

        /**
         * @param bool $admin
         */
        public function setAdmin(bool $admin): void
        {
            $this->admin = $admin;
        }

        /**
         * @return array|mixed
         */
        public function jsonSerialize() {
            return [
                'id'        => $this->getId(),
                'linkblue'  => $this->getLinkblue(),
                'admin'     => $this->isAdmin(),
            ];
        }
    }