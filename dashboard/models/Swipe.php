<?php
    require_once __DIR__ . '/../utilities/db.php';
    require_once __DIR__ . '/../utilities/UUID.php';

    class Swipe implements JsonSerializable {
        protected $id;
        protected $ts;
        protected $site;
        protected $swipe;
        protected $ukid;
        protected $linkblue;
        protected $fullname;

        public function __construct() {
            $this->id = UUID::v4();
        }

        public static function create($site, $swipe, $ukid, $linkblue, $fullname) {
            $instance = new self();
            $instance->setTs(new DateTime());
            $instance->setSite($site);
            $instance->setSwipe($swipe);
            $instance->setUkid($ukid);
            $instance->setLinkblue($linkblue);
            $instance->setFullName($fullname);
            $instance = $instance->save();
            return $instance;
        }

        public static function all() {
            $swipes = [];
            $query = "SELECT * FROM [swipe] s";
            $stmt = DB::run($query);
            while ($row = $stmt->fetch(PDO::FETCH_LAZY))
                array_push($swipes, Swipe::withRow($row));
            return $swipes;
        }

        public static function count_all() {
            $query = "SELECT count(id) as count FROM [swipe] s";
            $stmt = DB::run($query);
            $row = $stmt->fetch(PDO::FETCH_LAZY);
            if ($row <> null)
                return $row['count'];
            return 0;
        }

        public static function get_swipes($site, $from, $to, $search) {
            $swipes = array();
            $query = "SELECT * FROM [swipe]";
            $query .= " WHERE ts <= " . $to . " AND ts >= " . $from;
            if (!is_null($site) && $site <> 'all')
                $query .= " AND site = '" . $site . "'";
            if (!is_null($search) && $search <> '') {
                $search = strtoupper($search);
                $query .= " AND (UPPER(link_blue) LIKE '%" . $search . "%' OR UPPER(full_name) LIKE '%" . $search . "%')";
            }
            $stmt = DB::run($query);
            while ($row = $stmt->fetch(PDO::FETCH_LAZY))
                array_push($swipes, Swipe::withRow($row));
            return $swipes;
        }

        public static function get_dt_rows($start, $length, $from, $to, $site, $search, $order_by, $order_dir) {
            $swipes = array();
            $query = "SELECT id, ts, site, swipe, ukid, link_blue, full_name FROM [swipe]";
            $query .= " WHERE ts <= " . $to . " AND ts >= " . $from;
            if (!is_null($site) && $site <> 'all')
                $query .= " AND site = '" . $site . "'";
            if (!is_null($search) && $search <> '') {
                $search = strtoupper($search);
                $query .= " AND (UPPER(link_blue) LIKE '%" . $search . "%' OR UPPER(full_name) LIKE '%" . $search . "%')";
            }
            $query .= " ORDER BY " . $order_by . " " . $order_dir . " OFFSET " . $start . " ROWS FETCH NEXT " . $length . " ROWS ONLY";
            $stmt = DB::run($query);
            while ($row = $stmt->fetch(PDO::FETCH_LAZY))
                array_push($swipes, array(
                    'id'        => $row['id'],
                    'ts'        => $row['ts'],
                    'site'      => $row['site'],
                    'swipe'     => $row['swipe'],
                    'ukid'      => $row['ukid'],
                    'linkblue'  => $row['link_blue'],
                    'fullname'  => $row['full_name'],
                ));
            return $swipes;
        }

        public static function get_dt_count($from, $to, $site, $search) {
            $query = "SELECT count(id) as count FROM [swipe]";
            $query .= " WHERE ts <= " . $to . " AND ts >= " . $from;
            if (!is_null($site) && $site <> 'all')
                $query .= " AND site = '" . $site . "'";
            if (!is_null($search) && $search <> '') {
                $search = strtoupper($search);
                $query .= " AND (UPPER(link_blue) LIKE '%" . $search . "%' OR UPPER(full_name) LIKE '%" . $search . "%')";
            }
            $stmt = DB::run($query);
            $row = $stmt->fetch(PDO::FETCH_LAZY);
            if ($row <> null)
                return $row['count'];
            return 0;
        }

        public static function get_sites() {
            $sites = array();
            $stmt = DB::run(
                "SELECT distinct(site) FROM [swipe]"
            );
            while ($row = $stmt->fetch(PDO::FETCH_LAZY))
                array_push($sites, $row['site']);
            return $sites;
        }

        public static function withID($id) {
            try {
                $instance = new self();
                $instance->loadByID($id);
                return $instance;
            } catch (PDOException $e) {
                return null;
            }
        }

        public static function withRow($row) {
            $instance = new self();
            $instance->fill($row);
            return $instance;
        }

        protected function loadByID($id) {
            $stmt = DB::run(
                "SELECT * FROM [swipe] s WHERE s.id = :id ORDER BY s.id OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY",
                ['id' => $id]
            );
            $row = $stmt->fetch(PDO::FETCH_LAZY);
            if ($row <> null) {
                $this->fill($row);
            } else
                throw new PDOException("Swipe with id [{$id}] not found");
        }

        protected function fill($row) {
            $this->id = $row['id'];
            $this->ts = $row['ts'];
            $this->site = $row['site'];
            $this->swipe = $row['swipe'];
            $this->ukid = $row['ukid'];
            $this->linkblue = $row['link_blue'];
            $this->fullname = $row['full_name'];
        }

        protected function save() {
            $exists = Swipe::withID($this->getId());
            if (is_null($exists)) {
                $insert = "INSERT INTO [swipe] (id, ts, site, swipe, ukid, link_blue, full_name) VALUES (?,?,?,?,?,?,?)";
                DB::run($insert, [$this->getId(), $this->getTs(), $this->getSite(), $this->getSwipe(),
                    $this->getUkid(), $this->getLinkBlue(), $this->getFullName()]);
            } else {
                $update = "UPDATE [sessions] SET user_id=?, last_seen=GETDATE(), remember_me=? WHERE id=?";
                DB::run($update, [$this->getUserId(), $this->getRememberMe(), $this->getId()]);
            }
            return Swipe::withID($this->getId());
        }

        public static function delete($id) {
            $delete = "DELETE FROM [swipe] WHERE id = :id";
            DB::run($delete, ['id' => $id]);
        }

        /**
         * @return string
         */
        public function getId(): string {
            return $this->id;
        }

        /**
         * @param string $id
         */
        public function setId(string $id): void {
            $this->id = $id;
        }

        /**
         * @return mixed
         */
        public function getTs() {
            return $this->ts;
        }

        /**
         * @param mixed $ts
         */
        public function setTs($ts): void {
            $this->ts = $ts;
        }

        /**
         * @return mixed
         */
        public function getSite() {
            return $this->site;
        }

        /**
         * @param mixed $site
         */
        public function setSite($site): void {
            $this->site = $site;
        }

        /**
         * @return mixed
         */
        public function getSwipe() {
            return $this->swipe;
        }

        /**
         * @param mixed $swipe
         */
        public function setSwipe($swipe): void {
            $this->swipe = $swipe;
        }

        /**
         * @return mixed
         */
        public function getUkid() {
            return $this->ukid;
        }

        /**
         * @param mixed $ukid
         */
        public function setUkid($ukid): void {
            $this->ukid = $ukid;
        }

        /**
         * @return mixed
         */
        public function getLinkBlue() {
            return $this->linkblue;
        }

        /**
         * @param mixed $linkblue
         */
        public function setLinkBlue($linkblue): void {
            $this->linkblue = $linkblue;
        }

        /**
         * @return mixed
         */
        public function getFullName() {
            return $this->fullname;
        }

        /**
         * @param mixed $fullname
         */
        public function setFullName($fullname): void {
            $this->fullname = $fullname;
        }

        /**
         * @return array|mixed
         */
        public function jsonSerialize() {
            return [
                'id'        => $this->getId(),
                'ts'        => $this->getTs(),
                'site'      => $this->getSite(),
                'swipe'     => $this->getSwipe(),
                'ukid'      => $this->getUkid(),
                'linkblue'  => $this->getLinkblue(),
                'fullname'  => $this->getFullName(),
            ];
        }
    }