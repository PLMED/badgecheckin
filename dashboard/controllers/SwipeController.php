<?php

include_once MODELS_DIR . 'User.php';
include_once MODELS_DIR . 'Swipe.php';

class SwipeController {
    public static function sites($user) {
        $ret = array();
        $ret['sites'] = Swipe::get_sites();
        echo json_encode($ret);
    }

    public static function list_dt($user) {
        $ret = array();
        if (isset($_REQUEST) && !is_null($_REQUEST)) {
            $draw = $_REQUEST['draw'];
            $start = intval($_REQUEST['start']);
            $length = intval($_REQUEST['length']);
            $from = intval($_REQUEST['from']);
            $to = intval($_REQUEST['to']);
            $site = $_REQUEST['site'];
            $search = $_REQUEST['search']['value'];
            $search_is_regex = $_REQUEST['search']['regex'];
            $order_by_idx = $_REQUEST['order'][0]['column'];
            $order_by = 'ts';
            switch ($order_by_idx) {
                case '0':
                    $order_by = 'ts';
                    break;
                case '1':
                    $order_by = 'site';
                    break;
                case '2':
                    $order_by = 'swipe';
                    break;
                case '3':
                    $order_by = 'ukid';
                    break;
                case '4':
                    $order_by = 'link_blue';
                    break;
                case '5':
                    $order_by = 'full_name';
                    break;
            }
            $order_dir = $_REQUEST['order'][0]['dir'];
            $data = Swipe::get_dt_rows($start, $length, $from, $to, $site, $search, $order_by, $order_dir);

            $ret['draw'] = $draw;
            $ret['recordsTotal'] = Swipe::count_all();
            $ret['recordsFiltered'] = Swipe::get_dt_count($from, $to, $site, $search);
            $ret['data'] = $data;
        }
        echo json_encode($ret);
    }

    public static function download($user, $site, $from, $to, $filter) {
        $filename = "ukhc_pathology_swipes_" . $site . "_" . $from . "_" . $to . ".csv";
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=" . $filename);
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "ts,site,swipe,ukid,linkblue,fullname\n";
        $swipes = Swipe::get_swipes($site, $from, $to, $filter);
        foreach ($swipes as $swipe) {
            echo $swipe->getTs() . ',' . $swipe->getSite() . ',' . $swipe->getSwipe() . ',' . $swipe->getUkid() . ',' . $swipe->getLinkblue() . ',' . $swipe->getFullName() . "\n";
        }
    }

    public static function search($user) {
        require VIEWS_DIR . 'search.php';
    }
}

