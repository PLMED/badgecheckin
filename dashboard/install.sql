create table users
(
    id       varchar(36)   not null
        constraint user_pk
        primary key nonclustered,
    linkblue varchar(36)   not null,
    admin    bit default 0 not null
)
go

create table sessions
(
    id varchar(36) not null
        constraint session_pk
        primary key nonclustered,
    user_id     varchar(36)                not null
        constraint user_sessions_users_id_fk
        references users
        on delete cascade,
    last_seen   datetime default getdate() not null,
    remember_me bit      default 0         not null
)
go

create unique index user_linkblue_uindex
    on users (linkblue)
go

create table swipe
(
    id varchar(36) not null
        constraint swipe_pk
        primary key nonclustered,
    ts bigint,
    site varchar(255),
    swipe varchar(255),
    ukid varchar(255),
    link_blue varchar(255),
    full_name varchar(255)
)
go