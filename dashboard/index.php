<?php
    date_default_timezone_set('America/New_York');
    ini_set('display_startup_errors', 1);
    ini_set('display_errors', 1);
    error_reporting(-1);

    define('CONFIG_FILE', __DIR__ . '/config.php');
    define('MODELS_DIR', __DIR__ . '/models/');
    define('UTILITIES_DIR', __DIR__ . '/utilities/');
    define('VIEWS_DIR', __DIR__ . '/views/');

    $config = include 'config.php';

    require_once __DIR__ . '/vendor/autoload.php';

    session_start();

    require_once __DIR__ . '/routes.php';