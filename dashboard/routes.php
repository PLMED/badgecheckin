<?php
    include_once __DIR__ . '/controllers/RootController.php';
    include_once __DIR__ . '/controllers/SwipeController.php';

    function check_session($redirect = "/", $requires_admin = false) {
        $session = UserSession::withID(session_id());
        if (is_null($session))
            header('Location: /login');
        else {
            $user = User::withId($session->getUserId());
            if (is_null($user))
                header('Location: /logout');
            else if ($requires_admin && !$user->isAdmin())
                header('Location: /');
        }
        return $session;
    }

    function load_user($session) {
        return User::withId($session->getUserId());
    }

    $router = new AltoRouter();

    /* RootController routes */
    $router->map('GET', '/', function() {
        RootController::index(load_user(check_session()));
    }, 'index');
    $router->map('GET', '/login', function() {
        RootController::login();
    }, 'login');
    $router->map('POST', '/login', function() {
        try {
            RootController::do_login(strtolower($_POST['user']), $_POST['password'], 0);
        } catch (Exception $e) {
            $_SESSION['LOGIN_ERROR'] = $e->getMessage();
            header('Location: /login');
        }
    });
    $router->map('GET', '/logout', function() {
        RootController::logout();
    }, 'logout');

    /* SwipeController routes */
    $router->map('GET', '/swipes', function() {
        SwipeController::list_dt(load_user(check_session()));
    }, 'swipes');
    $router->map('GET', '/sites', function() {
        SwipeController::sites(load_user(check_session()));
    }, 'sites');
    $router->map('GET', '/download/[*:site]/[i:from]/[i:to]', function($site, $from, $to) {
        SwipeController::download(load_user(check_session()), $site, $from, $to, null);
    }, 'download');
    $router->map('GET', '/download/[*:site]/[i:from]/[i:to]/[*:filter]', function($site, $from, $to, $filter) {
        SwipeController::download(load_user(check_session()), $site, $from, $to, $filter);
    }, 'download-with-filter');

    /* DEBUG routes */
    $router->map('GET', '/info', function() {
        phpinfo();
    }, 'php-info');

    $match = $router->match();

    // Call closure or throw 404 status
    if ($match && is_callable($match['target'])) {
        call_user_func_array($match['target'], $match['params']);
    } else {
        // No route was matched
        header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
    }